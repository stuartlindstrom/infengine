#version 330

in vec2 texCoord;
in vec4 color;

out vec4 fragColor;

uniform sampler2D texture_sampler;

void main() {
	fragColor = color * texture(texture_sampler, texCoord);
}
#version 330

layout (location=0) in vec2 position;
layout (location=1) in vec2 vertexTexCoord;

out vec2 texCoord;

uniform mat4 orthoMatrix;

void main() {
	gl_Position = orthoMatrix * vec4(position, 0, 1.0);
	texCoord = vertexTexCoord;
}
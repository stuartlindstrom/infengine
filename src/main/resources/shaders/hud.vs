#version 330

layout (location=0) in vec3 position;
layout (location=1) in vec2 vertexTexCoord;

out vec2 texCoord;
out vec4 color;

uniform mat4 orthoMatrix;

void main() {
	gl_Position = orthoMatrix * vec4(position, 1.0);
	texCoord = vertexTexCoord;
	color = vec4(1.0, 1.0, 1.0, 1.0);
}
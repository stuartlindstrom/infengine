#version 330

layout (location=0) in vec3 position;
layout (location=1) in vec2 vertexTexCoord;
layout (location=2) in vec3 vertexLighting;

out vec3 lighting;
out vec2 texCoord;
out float cameraDistance;

uniform mat4 projectionMatrix;
uniform mat4 worldMatrix;

void main() {
	gl_Position = projectionMatrix * worldMatrix * vec4(position, 1.0);
	
	lighting = vertexLighting;
	
	//cameraDistance = gl_Position.z;
	cameraDistance = length(gl_Position);
	
	texCoord = vertexTexCoord;
}
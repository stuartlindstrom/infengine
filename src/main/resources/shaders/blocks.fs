#version 330

in vec3 lighting;
in vec2 texCoord;
in float cameraDistance;

out vec4 fragColor;

uniform sampler2D texture_sampler;

void main() {
	//fragColor = vec4(color, 1) * texture(texture_sampler, texCoord);
	
	float viewDist = 96;
	
	//float fogFactor = 1 - clamp(1 / exp(pow(cameraDistance / 96, 5.0)), 0.0, 1.0);
	float fogFactor = 1 - clamp(1 / exp(pow(cameraDistance / viewDist * 0.8, 7)), 0.0, 1.0);
	
	vec4 texColor = texture(texture_sampler, texCoord);
	fragColor = mix(vec4(lighting, 1) * texColor, vec4(0.625, 0.75, 1.0, 1.0), fogFactor);
}
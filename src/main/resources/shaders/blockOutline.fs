#version 330

in vec4 color;

out vec4 fragColor;

uniform float time;

void main() {
	fragColor = vec4(color.rgb, color.a * ((sin(time / 5) * 0.125) + 0.875));
}
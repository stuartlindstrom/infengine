package main.java.infengine.render;

import static org.lwjgl.opengl.GL30.*;

import java.util.ArrayList;
import java.util.List;

public class VertexFormat {
	
	public final int size;
	protected final VertexAttribute[] attributes;
	
	private VertexFormat(int size, VertexAttribute[] attributes) {
		this.attributes = attributes;
		this.size = size;
	}
	
	public void initAttributes(ShaderProgram program) {
		for (VertexAttribute attribute : attributes) {
			int location = program.getAttributeLocation(attribute.name);
			glVertexAttribPointer(location, attribute.count, GL_FLOAT, attribute.normalize, this.size, attribute.offset);
		}
	}
	
	public void enableAttributes(ShaderProgram program) {
		for (VertexAttribute attribute : attributes) {
			int location = program.getAttributeLocation(attribute.name);
			glEnableVertexAttribArray(location);
		}
	}
	
	public void disableAttributes(ShaderProgram program) {
		for (VertexAttribute attribute : attributes) {
			int location = program.getAttributeLocation(attribute.name);
			glDisableVertexAttribArray(location);
		}
	}
	
	
	public static final class Builder {
		
		private List<VertexAttribute> attributes = new ArrayList<>();
		private int byteCounter = 0;
		
		public void addAttribute(String name, int count, boolean normalize) {
			attributes.add(new VertexAttribute(name, count, byteCounter, normalize));
			byteCounter += (count * 4);
		}
		
		public void addAttribute(String name, int count) {
			this.addAttribute(name, count, false);
		}
		
		public VertexFormat build() {
			return new VertexFormat(byteCounter, new VertexAttribute[attributes.size()]);
		}
		
	}

	
	private static class VertexAttribute {
		
		final String name;
		final int count, offset;
		final boolean normalize;
		
		VertexAttribute(String name, int count, int offset, boolean normalize) {
			this.name = name;
			this.count = count;
			this.offset = offset;
			this.normalize = normalize;
		}
		
	}
	
}

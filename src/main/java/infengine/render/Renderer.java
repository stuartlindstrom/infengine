package main.java.infengine.render;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector3i;

import main.java.infengine.gui.Hud;
import main.java.infengine.model.ChunkMesh;
import main.java.infengine.model.Mesh;
import main.java.infengine.util.StringIOUtils;
import main.java.infengine.world.World;
import main.java.infengine.world.chunk.Chunk;

public class Renderer {
	
	public static final float FOV = (float) Math.toRadians(60.0f);
    public static final float Z_NEAR = 0.01f;
    public static final float Z_FAR = 1000.f;
	
	private ShaderProgram blockShader;
	private Transformation transformation;
	private FrustumCullingFilter frustumCullingFilter;
	
	private Hud hud;
	
	private BlockSelectionRenderer blockSelectionRenderer;
	
	public Renderer() {
		this.transformation = new Transformation();
		this.frustumCullingFilter = new FrustumCullingFilter();
	}
	
	public void init(Window window) throws Exception {
		this.blockShader = new ShaderProgram();
		this.blockShader.createVertexShader(StringIOUtils.loadResource("/main/resources/shaders/blocks.vs"));
		this.blockShader.createFragmentShader(StringIOUtils.loadResource("/main/resources/shaders/blocks.fs"));
		this.blockShader.link();
		
		this.blockShader.createUniform("projectionMatrix");
		this.blockShader.createUniform("worldMatrix");
		
		this.blockShader.createUniform("texture_sampler");
		
		this.hud = new Hud(window);
		
		this.blockSelectionRenderer = new BlockSelectionRenderer();
		this.blockSelectionRenderer.init(window);
	}
	
	public void clear() {
		glClearColor(0.625f, 0.75f, 1f, 0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	
	public void render(Window window, World world, Camera camera, float partialTicks) {
		clear();
		
		if (window.isResized()) {
	        glViewport(0, 0, window.getWidth(), window.getHeight());
	        window.setResized(false);
	    }
		
		this.renderWorld(window, world, camera);
		
		this.blockSelectionRenderer.render(window, camera, transformation, partialTicks);
		
		this.renderHud(window);
		
	}
	
	Vector3f positionVec = new Vector3f(0, 0, 0);
	Vector3f rotationVec = new Vector3f(0, 0, 0);
	public void renderWorld(Window window, World world, Camera camera) {
		//glEnable(GL_SAMPLES);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		
		this.blockShader.bind();
		
		Matrix4f projectionMatrix = transformation.getProjectionMatrix(FOV, window.getWidth(), window.getHeight(), Z_NEAR, Z_FAR);
		Matrix4f viewMatrix = camera.getViewMatrix();
		
		frustumCullingFilter.updateFrustum(projectionMatrix, viewMatrix);
		
		for (Chunk chunk : world.loadedChunks.values()) {
			if (chunk.getChunkMesh().isEmpty()) continue;
			
			Vector3i blockPos = chunk.getPosition().getBlockCoord();
			positionVec.x = blockPos.x; positionVec.y = blockPos.y; positionVec.z = blockPos.z;
			Matrix4f worldMatrix = transformation.getWorldMatrix(positionVec, rotationVec, 1f, viewMatrix);
			
			if (!frustumCullingFilter.insideFrustum(blockPos.x, blockPos.y, blockPos.z, blockPos.x + Chunk.width, blockPos.y + Chunk.width, blockPos.z + Chunk.width)) {
				continue;
			}
			
			blockShader.setUniform("projectionMatrix", projectionMatrix);
			blockShader.setUniform("worldMatrix", worldMatrix);
					
			blockShader.setUniform("texture_sampler", 0);
			
			chunk.getChunkMesh().render();
		}
		
		this.blockShader.unbind();
	}
	
	public void renderHud(Window window) {
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		this.hud.render(window, transformation);
	}
	
	public void cleanup() {
		if (this.blockShader != null) {
			this.blockShader.cleanup();
        }
		if (this.hud != null) {
			this.hud.cleanup();
		}
		if (this.blockSelectionRenderer != null) {
			this.blockSelectionRenderer.cleanup();
		}
	}

}

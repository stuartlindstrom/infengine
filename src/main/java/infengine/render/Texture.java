package main.java.infengine.render;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.stb.STBImage.*;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.system.MemoryStack;

public class Texture {
	
	private final int id;
	private int width;
	private int height;
	
	public Texture(String filePath) throws Exception {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			IntBuffer w = stack.mallocInt(1);
			IntBuffer h = stack.mallocInt(1);
			IntBuffer comp = stack.mallocInt(1);
			
			//stbi_set_flip_vertically_on_load(true);
			ByteBuffer image = stbi_load(filePath, w, h, comp, 4);
			if (image == null) {
			    throw new RuntimeException("Failed to load texture file: "
			    		+ filePath
			            + System.lineSeparator() + stbi_failure_reason());
			}
			this.width = w.get();
			this.height = h.get();
			
			this.id = glGenTextures();
			
			glBindTexture(GL_TEXTURE_2D, this.id);
			
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
			//glGenerateMipmap(GL_TEXTURE_2D);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
			//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -2f);
		}
	}
	
	public void bind() {
        glBindTexture(GL_TEXTURE_2D, this.id);
    }
	
	public void cleanup() {
        glDeleteTextures(this.id);
    }

}

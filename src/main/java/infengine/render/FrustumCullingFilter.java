package main.java.infengine.render;

import org.joml.FrustumIntersection;
import org.joml.Matrix4f;

public class FrustumCullingFilter {
	
	private final Matrix4f prjViewMatrix;
    private FrustumIntersection frustumInt;

    public FrustumCullingFilter() {
        prjViewMatrix = new Matrix4f();
        frustumInt = new FrustumIntersection();
    }

    public void updateFrustum(Matrix4f projMatrix, Matrix4f viewMatrix) {
        // Calculate projection view matrix
        prjViewMatrix.set(projMatrix);
        prjViewMatrix.mul(viewMatrix);
        
        // Update frustum intersection class
        frustumInt.set(prjViewMatrix);
    }
    
    public boolean insideFrustum(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) {
        return frustumInt.testAab(minX, minY, minZ, maxX, maxY, maxZ);
    }
    
}

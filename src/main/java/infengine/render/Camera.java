package main.java.infengine.render;

import org.joml.Matrix4f;
import org.joml.Vector3d;
import org.joml.Vector3f;

import main.java.infengine.InfEngine;

public class Camera {
	
	private Vector3d position;
	private Vector3d rotation;
	private Matrix4f viewMatrix = new Matrix4f();
	
	private Vector3f dir = new Vector3f(0, 0, 0);
	
	public Camera() {
        this.position = new Vector3d(0, 0, 0);
        this.rotation = new Vector3d(0, 0, 0);
    }
	
	public Vector3d getPosition() {
        return this.position;
    }

    public void setPosition(double x, double y, double z) {
        this.position.x = x;
        this.position.y = y;
        this.position.z = z;
    }
    
    public void move(double dX, double dY, double dZ) {
        if ( dZ != 0 ) {
            position.x += (float) Math.sin(Math.toRadians(rotation.y)) * -dZ;
            position.z += (float) Math.cos(Math.toRadians(rotation.y)) * dZ;
        }
        if ( dX != 0) {
            position.x += (float) Math.sin(Math.toRadians(rotation.y - 90)) * -dX;
            position.z += (float) Math.cos(Math.toRadians(rotation.y - 90)) * dX;
        }
        position.y += dY;
    }
    
    public Vector3d getRotation() {
        return rotation;
    }

    public void setRotation(double x, double y, double z) {
        this.rotation.x = x;
        this.rotation.y = y;
        this.rotation.z = z;
    }

    public void rotate(double roll, double yaw, double pitch) {
        this.rotation.x += roll;
        this.rotation.y += yaw;
        this.rotation.z += pitch;
    }
    
    public Vector3f getLookVec() {
    	dir = this.viewMatrix.positiveZ(dir).negate();
    	return dir;
    }
    
    private Vector3f xRotVec = new Vector3f(1, 0, 0);
	private Vector3f yRotVec = new Vector3f(0, 1, 0);
	private Vector3f zRotVec = new Vector3f(0, 0, 1);
	public void updateViewMatrix() {
	    this.viewMatrix.identity();
	    
	    xRotVec.set(1, 0, 0);
	    yRotVec.set(0, 1, 0);
	    zRotVec.set(0, 0, 1);
	    
	    this.viewMatrix.rotate((float) Math.toRadians(rotation.x), xRotVec).
	    		rotate((float) Math.toRadians(rotation.y), yRotVec).
	    		rotate((float) Math.toRadians(rotation.z), zRotVec);
	    this.viewMatrix.translate((float) -position.x, (float) -position.y, (float) -position.z);
	}
	
	public Matrix4f getViewMatrix() {
		return this.viewMatrix;
	}

}

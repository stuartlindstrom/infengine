package main.java.infengine.render;

import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL30.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.system.MemoryUtil;

public class BufferRenderer {
	
	public final VertexFormat vertexFormat;
	protected final ShaderProgram shaderProgram;
	protected final int vaoId, vboId, eboId;
	protected int vertexCount;
	protected FloatBuffer vertexData;
	protected IntBuffer vertexOrder;
	
	public BufferRenderer(VertexFormat vertexFormat, ShaderProgram shaderProgram, int initialSize) {
		this.vertexData = MemoryUtil.memAllocFloat(initialSize);
		this.vertexOrder = MemoryUtil.memAllocInt(initialSize);
		
		this.vertexFormat = vertexFormat;
		this.shaderProgram = shaderProgram;
		
		this.vaoId = glGenVertexArrays();
		glBindVertexArray(this.vaoId);
		
		this.vboId = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vboId);
		glBufferData(GL_ARRAY_BUFFER, vertexData, GL_DYNAMIC_DRAW);
		vertexFormat.initAttributes(shaderProgram);
		
		this.eboId = glGenBuffers();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, vertexOrder, GL_DYNAMIC_DRAW);
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	
	// TODO
	public void bufferData(FloatBuffer vertexData, IntBuffer vertexOrder) {
		// Make sure internal buffers are large enough and resize them if necessary
		
		// Add data from argument buffers to internal buffers, ignoring empty portions of argument buffers
		
		// Orphan the buffer on the GPU and allocate a new one for the new data
	}
	
	// TODO
	public void sortFaces() {
		
	}
	
	public void render() {
		glBindVertexArray(vaoId);
		
		vertexFormat.enableAttributes(shaderProgram);
		
		glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_INT, 0);
		
		vertexFormat.enableAttributes(shaderProgram);
		
		glBindVertexArray(0);
	}
	
	public void cleanup() {
		MemoryUtil.memFree(vertexData);
		MemoryUtil.memFree(vertexOrder);
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDeleteBuffers(vboId);
		glDeleteBuffers(eboId);
		
		glBindVertexArray(0);
        glDeleteVertexArrays(vaoId);
	}

}

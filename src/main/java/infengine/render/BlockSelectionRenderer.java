package main.java.infengine.render;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector3i;
import org.lwjgl.system.MemoryUtil;

import main.java.infengine.InfEngine;
import main.java.infengine.block.Blocks;
import main.java.infengine.util.StringIOUtils;

public class BlockSelectionRenderer {
	
	protected final int vaoId;
	protected final List<Integer> vboIds = new ArrayList<Integer>();
	
	protected final int vertexCount;
	
	private ShaderProgram shader;
	
	public BlockSelectionRenderer() {
		float min = -0.002F, max = 1.002F, mid = 0.5f, innerMin = 0.09f, innerMax = 0.91f;
		float r = 1f, g = 1f, b = 0.9f, a = 0.625f;
		//float min = -0.002F, max = 1.002F, mid = 0.5f, innerMin = 0.0625f, innerMax = 0.9375f;
		//float r = 0.1f, g = 0.1f, b = 0.1f, a = 0.75f;
		float[] vertices = new float[] {
				min, min, max, min, min, min, max, min, min, max, min, max,
				innerMin, min, innerMax, innerMin, min, innerMin, innerMax, min, innerMin, innerMax, min, innerMax, 
				
				min, max, min, min, max, max, max, max, max, max, max, min,
				innerMin, max, innerMin, innerMin, max, innerMax, innerMax, max, innerMax, innerMax, max, innerMin,
				
				max, max, min, max, min, min, min, min, min, min, max, min,
				innerMax, innerMax, min, innerMax, innerMin, min, innerMin, innerMin, min, innerMin, innerMax, min,
				
				min, max, max, min, min, max, max, min, max, max, max, max,
				innerMin, innerMax, max, innerMin, innerMin, max, innerMax, innerMin, max, innerMax, innerMax, max,
				
				min, max, min, min, min, min, min, min, max, min, max, max,
				min, innerMax, innerMin, min, innerMin, innerMin, min, innerMin, innerMax, min, innerMax, innerMax,
				
				max, max, max, max, min, max, max, min, min, max, max, min,
				max, innerMax, innerMax, max, innerMin, innerMax, max, innerMin, innerMin, max, innerMax, innerMin,
		};
		float[] colors = new float[] {
				r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, 0, r, g, b, 0, r, g, b, 0, r, g, b, 0,
				r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, 0, r, g, b, 0, r, g, b, 0, r, g, b, 0,
				r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, 0, r, g, b, 0, r, g, b, 0, r, g, b, 0,
				r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, 0, r, g, b, 0, r, g, b, 0, r, g, b, 0,
				r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, 0, r, g, b, 0, r, g, b, 0, r, g, b, 0,
				r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, 0, r, g, b, 0, r, g, b, 0, r, g, b, 0,
		};
		int[] indices = new int[] {
				0, 1, 4, 4, 1, 5, 1, 2, 5, 5, 2, 6, 2, 3, 6, 6, 3, 7, 3, 0, 7, 7, 0, 4,
				8, 9, 12, 12, 9, 13, 9, 10, 13, 13, 10, 14, 10, 11, 14, 14, 11, 15, 11, 8, 15, 15, 8, 12,
				16, 17, 20, 20, 17, 21, 17, 18, 21, 21, 18, 22, 18, 19, 22, 22, 19, 23, 19, 16, 23, 23, 16, 20,
				24, 25, 28, 28, 25, 29, 25, 26, 29, 29, 26, 30, 26, 27, 30, 30, 27, 31, 27, 24, 31, 31, 24, 28,
				32, 33, 36, 36, 33, 37, 33, 34, 37, 37, 34, 38, 34, 35, 38, 38, 35, 39, 35, 32, 39, 39, 32, 36,
				40, 41, 44, 44, 41, 45, 41, 42, 45, 45, 42, 46, 42, 43, 46, 46, 43, 47, 43, 40, 47, 47, 40, 44,
		};
		
		FloatBuffer vertexBuffer = null;
		FloatBuffer colorBuffer = null;
		IntBuffer indexBuffer = null;
		try {
			vertexBuffer = MemoryUtil.memAllocFloat(vertices.length);
			vertexBuffer.put(vertices).flip();
			
			colorBuffer = MemoryUtil.memAllocFloat(colors.length);
			colorBuffer.put(colors).flip();
			
			//this.vertexCount = vertices.length / 3;
			indexBuffer = MemoryUtil.memAllocInt(indices.length);
			this.vertexCount = indices.length; 
			indexBuffer.put(indices).flip();
		
			// Create the vao and bind opengl to it
			this.vaoId = glGenVertexArrays();
			glBindVertexArray(this.vaoId);
		
			int vboId;
			
			// Creates the vertex vbo
			vboId = glGenBuffers();
			this.vboIds.add(vboId);
			glBindBuffer(GL_ARRAY_BUFFER, vboId);
			glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			
			// Creates the color vbo
			vboId = glGenBuffers();
			this.vboIds.add(vboId);
			glBindBuffer(GL_ARRAY_BUFFER, vboId);
			glBufferData(GL_ARRAY_BUFFER, colorBuffer, GL_STATIC_DRAW);
			glVertexAttribPointer(1, 4, GL_FLOAT, false, 0, 0);
			
			// Creates the index vbo
			vboId = glGenBuffers();
			this.vboIds.add(vboId);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL_STATIC_DRAW);
		
			// Unbind the vao and vbo objects from opengl
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		} finally {
			if (vertexBuffer != null) {
				MemoryUtil.memFree(vertexBuffer);
			}
			if (colorBuffer != null) {
				MemoryUtil.memFree(colorBuffer);
			}
			if (indexBuffer != null) {
				MemoryUtil.memFree(indexBuffer);
			}
		}
	}
	
	public void init(Window window) throws Exception {
		this.shader = new ShaderProgram();
		this.shader.createVertexShader(StringIOUtils.loadResource("/main/resources/shaders/blockOutline.vs"));
		this.shader.createFragmentShader(StringIOUtils.loadResource("/main/resources/shaders/blockOutline.fs"));
		this.shader.link();
		
		this.shader.createUniform("projectionMatrix");
		this.shader.createUniform("worldMatrix");
		this.shader.createUniform("time");
	}
	
	Vector3f positionVec = new Vector3f(0, 0, 0);
	Vector3f rotationVec = new Vector3f(0, 0, 0);
	public void render(Window window, Camera camera, Transformation transformation, float partialTicks) {
		Vector3i blockPos = InfEngine.infEngine.selectedBlock;
		if (InfEngine.infEngine.world != null && InfEngine.infEngine.hasSelectedBlock && InfEngine.infEngine.world.getBlockID(blockPos.x, blockPos.y, blockPos.z) != Blocks.AIR) {
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			
			this.shader.bind();
		
			Matrix4f projectionMatrix = transformation.getProjectionMatrix(Renderer.FOV, window.getWidth(), window.getHeight(), Renderer.Z_NEAR, Renderer.Z_FAR);
			Matrix4f viewMatrix = camera.getViewMatrix();
			positionVec.set(blockPos.x, blockPos.y, blockPos.z);
			Matrix4f worldMatrix = transformation.getWorldMatrix(positionVec, rotationVec, 1f, viewMatrix);
			this.shader.setUniform("projectionMatrix", projectionMatrix);
			this.shader.setUniform("worldMatrix", worldMatrix);
			this.shader.setUniform("time", InfEngine.infEngine.world.getTime() + partialTicks);
			
			glBindVertexArray(this.vaoId);
			
			// Enable the coordinate vbo
			glEnableVertexAttribArray(0);
			// Enable the color vbo
			glEnableVertexAttribArray(1);
			
			glDrawElements(GL_TRIANGLES, this.vertexCount, GL_UNSIGNED_INT, 0);
			//glDrawArrays(GL_LINES, 0, vertexCount);
			
			// Disable the coordinate vbo
			glDisableVertexAttribArray(0);
			// Disable the color vbo
			glDisableVertexAttribArray(1);
			
			glBindVertexArray(0);
			
			this.shader.unbind();
		}
	}
	
	public void cleanup() {
		if (this.shader != null) {
			this.shader.cleanup();
        }
		
		glDisableVertexAttribArray(0);
		
		// Delete the vbos
		for (int vboId : this.vboIds) {
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glDeleteBuffers(vboId);
		}
		
		// Delete the vao
		glBindVertexArray(0);
        glDeleteVertexArrays(vaoId);
	}

}

package main.java.infengine;

import org.joml.Intersectionf;
import org.joml.Vector2d;
import org.joml.Vector2f;
import org.joml.Vector3d;
import org.joml.Vector3f;
import org.joml.Vector3i;
import org.lwjgl.glfw.GLFW;

import main.java.infengine.block.Block;
import main.java.infengine.block.Blocks;
import main.java.infengine.entity.Entity;
import main.java.infengine.entity.mob.player.EntityPlayer;
import main.java.infengine.gui.FontRenderer;
import main.java.infengine.input.KeyInput;
import main.java.infengine.input.MouseInput;
import main.java.infengine.model.ChunkMesh;
import main.java.infengine.render.Camera;
import main.java.infengine.render.Renderer;
import main.java.infengine.render.Window;
import main.java.infengine.util.AxisAlignedBB;
import main.java.infengine.util.Direction;
import main.java.infengine.util.MathUtil;
import main.java.infengine.util.Timer;
import main.java.infengine.world.World;
import main.java.infengine.world.chunk.Chunk;

public class InfEngine {

	public static InfEngine infEngine;
	static {
		try {
			infEngine = new InfEngine("InfEngine", 1080, 720, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static final int TARGET_FPS = 60;
	public static final int TARGET_TPS = 20;

	private final Window window;
	private final Renderer renderer;
	private final Timer timer;
	private final Camera camera;
	
	public final MouseInput mouseInput;
	public final KeyInput keyInput;
	
	public FontRenderer fontRenderer;
	
	public World world;
	public EntityPlayer player;
	
	public static boolean freeMouse = false;
	
	public Vector3i selectedBlock = new Vector3i(0, 0, 0);
	public boolean hasSelectedBlock = false;
	public Direction selectedBlockSide = Direction.UNDEFINED;

	private InfEngine(String windowTitle, int width, int height, boolean vSync) throws Exception {
		window = new Window(width, height, windowTitle, true);
		timer = new Timer();
		renderer = new Renderer();
		camera = new Camera();
		
		mouseInput = new MouseInput();
		keyInput = new KeyInput();
	}

	public void start() {
		run();
	}

	public void run() {
		try {
			init();
			gameLoop();
		} catch (Exception excp) {
			excp.printStackTrace();
		} finally {
			cleanup();
		}
	}
	
	protected void init() throws Exception {
		window.init();
		
		timer.init();
		
		fontRenderer = new FontRenderer();
		renderer.init(window);
		
		mouseInput.init(window);
		keyInput.init(window);
		
		int spawnX = 0, spawnZ = 0;
		
		this.world = new World();
		this.player = new EntityPlayer(world);
		world.addEntity(player);
		player.y = world.getTerrainHeight(spawnX, spawnZ) + 64;
		player.x = spawnX + 0.5;
		player.z = spawnZ + 0.5;
		player.setHasGravity(true);
		player.setNoClip(false);
		
		// Print key controls
		System.out.println("ESC - Exit Game\nE - Free Cursor\nF - Toggle Flight\nC - Toggle Collisions\nO - Print Seed\n");
	}

	private int framesPerSecond;
	
	public int getFPS() {return this.framesPerSecond;}
	
	protected void gameLoop() {
		float elapsedTime;
		float accumulator = 0f;
		float interval = 1f / TARGET_TPS;

		int frames = 0;
		float fpsAccumulator = 0;
		
		boolean running = true;
		while (running && !window.windowShouldClose()) {
			elapsedTime = timer.getElapsedTime();
			accumulator += elapsedTime;

			input();
			
			while (accumulator >= interval) {
				this.tick(interval);
				accumulator -= interval;
			}
			
			long startTime = System.currentTimeMillis();
			
			float partialTicks = accumulator / interval;
			render(partialTicks);
			
			//System.out.println("Rendering took: " + (System.currentTimeMillis() - startTime) + " millis");
			
			frames++;
			fpsAccumulator += elapsedTime;
			if (fpsAccumulator > 1) {
				this.framesPerSecond = frames;
				// System.out.println(frames);
				frames = 0;
				fpsAccumulator -= 1;
			}

			if (!window.isvSync()) {
				sync();
			}
		}
	}

	private void sync() {
		float loopSlot = 1f / TARGET_FPS;
		double endTime = timer.getLastLoopTime() + loopSlot;
		while (timer.getTime() < endTime) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException ie) {
			}
		}
	}

	public void cleanup() {
		if (world != null) world.cleanup();
		if (renderer != null) renderer.cleanup();
		if (window != null) window.cleanup();
		if (ChunkMesh.BLOCK_TEXTURES != null) ChunkMesh.BLOCK_TEXTURES.cleanup();
		if (fontRenderer != null) fontRenderer.cleanup();
	}

	private final Vector3f playerInput = new Vector3f(0, 0, 0);
	protected void input() {
		if (keyInput.wPressed() && !keyInput.sPressed()) {
			playerInput.z = -1;
		} else if (keyInput.sPressed() && !keyInput.wPressed()) {
			playerInput.z = 1;
		} else {
			playerInput.z = 0;
		}
		if (keyInput.aPressed() && !keyInput.dPressed()) {
			playerInput.x = -1;
		} else if (keyInput.dPressed() && !keyInput.aPressed()) {
			playerInput.x = 1;
		} else {
			playerInput.x = 0;
		}
		if (keyInput.shiftPressed() && !keyInput.spacePressed()) {
			playerInput.y = -1;
		} else if (keyInput.spacePressed() && !keyInput.shiftPressed()) {
			if (!player.hasGravity()) {
				playerInput.y = 1;
			}
		} else {
			playerInput.y = 0;
		}
		
		if (playerInput.lengthSquared() > 0) {
			playerInput.normalize();
		}
	}

	private int blockBreakDelay = 0;
	private int blockPlaceDelay = 0;
	
	public int currentBlockID = Blocks.COBBLESTONE;
	
	protected void tick(float interval) {
		if (!freeMouse) { // only do all of this when the mouse is hidden for first person mode
			// do player movement based on data gathered from key input
			player.move(playerInput.x, (!player.hasGravity()) ? playerInput.y : 0, playerInput.z);
			if (player.hasGravity() && keyInput.isKeyPressed(GLFW.GLFW_KEY_SPACE, false)) {
				player.jump();
			}
			
			if (keyInput.isKeyPressed(GLFW.GLFW_KEY_O, true)) {
				System.out.println(world.getSeed());
			}
			
			// modify the player's head rotation (and indirectly the camera rotation) based on mouse movement
			float mouseSensitivity = 0.5f;
			Vector2d mousePos = mouseInput.getPos();
			Vector2d prevMousePos = mouseInput.getPrevPos();
			
			player.headPitch += (mousePos.y - prevMousePos.y) * mouseSensitivity;
			player.headYaw += (mousePos.x - prevMousePos.x) * mouseSensitivity;;
			
			// Change the held block when mouse wheel is scrolled
			int mouseScroll = (int) mouseInput.getScrollOffset();
			if (mouseScroll != 0) {
				this.currentBlockID = 1 + MathUtil.mod(this.currentBlockID + mouseScroll - 1, Blocks.BLOCKS.length - 1);
			}
			
			// decrement placement and destruction delays
			if (blockBreakDelay > 0) blockBreakDelay--;
			if (blockPlaceDelay > 0) blockPlaceDelay--;
			// recalculate the position of the block the player is looking at
			selectBlock();
			// middle click to select the block the player is looking at
			if (mouseInput.middleButtonPressed() && this.selectedBlock != null && this.hasSelectedBlock) {
				this.currentBlockID = world.getBlockID(this.selectedBlock.x, this.selectedBlock.y, this.selectedBlock.z);
			}
			// break blocks on left mouse
			if (mouseInput.leftButtonPressed() && hasSelectedBlock && blockBreakDelay == 0) {
				world.setBlock(Blocks.AIR, selectedBlock.x, selectedBlock.y, selectedBlock.z, true);
				blockBreakDelay = 4;
			}
			// place a block on right mouse
			if (mouseInput.rightButtonPressed() && hasSelectedBlock && blockPlaceDelay == 0 && selectedBlockSide != Direction.UNDEFINED) {
				int blockX = selectedBlock.x;
				int blockY = selectedBlock.y;
				int blockZ = selectedBlock.z;
				// get the location to place a block at based on the position and face of the block the player is looking at
				switch (selectedBlockSide) {
				case DOWN:
					blockY--;
					break;
				case UP:
					blockY++;
					break;
				case NORTH:
					blockZ--;
					break;
				case SOUTH:
					blockZ++;
					break;
				case WEST:
					blockX--;
					break;
				case EAST:
					blockX++;
					break;
				default:
					break;
				}
				// make sure there isn't already a block at the location
				if (world.getBlockID(blockX, blockY, blockZ) == Blocks.AIR) {
					boolean obstructed = false;
					Block placing = Blocks.BLOCKS[currentBlockID];
					// make sure there aren't any entities occupying the position the block will be placed at
					for (Entity e : world.loadedEntities.values()) {
						if (e.getCollisionAABB().intersectsWithOffsets(placing.getCollisionAABB(), 0, 0, 0, blockX, blockY, blockZ)) {
							obstructed = true;
							break;
						}
					}
					if (!obstructed) {
						world.setBlock(this.currentBlockID, blockX, blockY, blockZ, true);
						blockPlaceDelay = 4;
					}
				}
			}
			
			this.world.update();
		}
		mouseInput.update(window);
	}
	
	private static final float REACH_DISTANCE = 20.5F;
	private Vector3f hitVec = new Vector3f(0, 0, 0);
	private Vector2f lookIntersectResult = new Vector2f(0, 0); // holds the distance to the near and far points of the intersection
	private void selectBlock() {
		Vector3f lookVec = camera.getLookVec();
		Vector3d cameraPos = camera.getPosition();
		
		selectedBlock.set((int) Math.floor(cameraPos.x), (int) Math.floor(cameraPos.y), (int) Math.floor(cameraPos.z));
		hasSelectedBlock = false;
		
		Block selectedBlockType = Blocks.BLOCKS[Blocks.AIR];
		float closestDist = REACH_DISTANCE;
		
		for (Chunk chunk : world.loadedChunks.values()) {
			if (chunk.getAABB().intersectsRay(cameraPos, lookVec, lookIntersectResult) && lookIntersectResult.x <= closestDist) {
				for (int i = 0; i < chunk.getBlocks().length; i++) {
					int blockId = chunk.getBlocks()[i];
					Block block = Blocks.BLOCKS[blockId];
					int z = i / (Chunk.layerSize);
					int y = (i - (z * Chunk.layerSize)) / Chunk.width;
					int x = i - ((z * Chunk.layerSize) + (y * Chunk.width));
					z += chunk.getPosition().getBlockZ();
					y += chunk.getPosition().getBlockY();
					x += chunk.getPosition().getBlockX();
					
					AxisAlignedBB blockAABB = block.getCollisionAABB();
					if (Intersectionf.intersectRayAab((float) cameraPos.x, (float) cameraPos.y, (float) cameraPos.z, lookVec.x, lookVec.y, lookVec.z, (float) blockAABB.minX + x, (float) blockAABB.minY + y, (float) blockAABB.minZ + z, (float) blockAABB.maxX + x, (float) blockAABB.maxY + y, (float) blockAABB.maxZ + z, lookIntersectResult)) {
						if (lookIntersectResult.x > 0.05F && lookIntersectResult.x <= closestDist) {
							hasSelectedBlock = true;
							closestDist = lookIntersectResult.x;
							selectedBlock.set(x, y, z);
							selectedBlockType = block;
						}
					}
				}
			}
		}
		
		if (hasSelectedBlock) {
			hitVec = hitVec.set(lookVec).mul(closestDist);
			double x = cameraPos.x + hitVec.x, y = cameraPos.y + hitVec.y, z = cameraPos.z + hitVec.z;

			selectedBlockSide = selectedBlockType.selects(cameraPos.x, cameraPos.y, cameraPos.z, cameraPos.x + (lookVec.x * REACH_DISTANCE), cameraPos.y + (lookVec.y * REACH_DISTANCE), cameraPos.z + (lookVec.z * REACH_DISTANCE), selectedBlock.x, selectedBlock.y, selectedBlock.z);
			if (selectedBlockSide == Direction.UNDEFINED) {
				hasSelectedBlock = false;
			}
		}
	}

	protected void render(float partialTicks) {
		camera.setPosition(player.prevX + ((player.x - player.prevX) * partialTicks), player.prevY + ((player.y - player.prevY) * partialTicks) + player.getEyeHeight(), player.prevZ + ((player.z - player.prevZ) * partialTicks));
		camera.setRotation((player.prevPitch + ((player.pitch - player.prevPitch) * partialTicks)) + (player.prevHeadPitch + ((player.headPitch - player.prevHeadPitch) * partialTicks)),
				(player.prevYaw + ((player.yaw - player.prevYaw) * partialTicks)) + (player.prevHeadYaw + ((player.headYaw - player.prevHeadYaw) * partialTicks)),
				(player.prevRoll + ((player.roll - player.prevRoll) * partialTicks)) + (player.prevHeadRoll + ((player.headRoll - player.prevHeadRoll) * partialTicks)));
		camera.updateViewMatrix();
		
		renderer.render(window, world, camera, partialTicks);
		window.update();
	}
	
	public Vector3d getCameraPosition() {
		return this.camera.getPosition();
	}

}

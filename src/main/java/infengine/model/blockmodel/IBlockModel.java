package main.java.infengine.model.blockmodel;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

import org.joml.Vector3i;

import main.java.infengine.util.Direction;

public interface IBlockModel {
	
	public void addFaceQuadsToBuffer(Vector3i pos, Direction face, FloatBuffer vertices, FloatBuffer texCoords, FloatBuffer lighting, IntBuffer indices);

}

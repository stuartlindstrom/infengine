package main.java.infengine.model.blockmodel;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

import org.joml.Vector3i;

import main.java.infengine.util.Direction;
import main.java.infengine.world.chunk.Chunk;

public class BasicBlockModel implements IBlockModel {

	private static final float[][] basic_vertices = new float[][] {
		{0f, 0f, 1f, 0f, 0f, 0f, 1f, 0f, 0f, 1f, 0f, 1f}, // down
		{0f, 1f, 0f, 0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 0f}, // up
		{1f, 1f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f}, // north
		{0f, 1f, 1f, 0f, 0f, 1f, 1f, 0f, 1f, 1f, 1f, 1f}, // south
		{0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 1f, 1f}, // west
		{1f, 1f, 1f, 1f, 0f, 1f, 1f, 0f, 0f, 1f, 1f, 0f}, // east
	};
	private static final float[] basic_texcoords = new float[] { 0f, 0f, 0f, 1f, 1f, 1f, 1f, 0f };
	private static final float[][] basic_lighting = new float[][] {
		{0.6f, 0.6f, 0.6f, 0.6f, 0.6f, 0.6f, 0.6f, 0.6f, 0.6f, 0.6f, 0.6f, 0.6f},
		{1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f},
		{0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f},
		{0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f, 0.85f},
		{0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f},
		{0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f, 0.75f},
	};
	private static final int[] basic_indices = new int[] { 0, 1, 3, 3, 1, 2 };
	
	private BlockQuad[] quads = new BlockQuad[6];

	public BasicBlockModel(int textureIndex) {
		for (int i = 0; i < 6; i++) {
			this.quads[i] = new BlockQuad(basic_vertices[i], basic_texcoords, basic_lighting[i], textureIndex, basic_indices);
		}
	}
	
	public BasicBlockModel(int downTexIndex, int upTexIndex, int northTexIndex, int southTexIndex, int westTexIndex, int eastTexIndex) {
		this.quads[0] = new BlockQuad(basic_vertices[0], basic_texcoords, basic_lighting[0], downTexIndex, basic_indices);
		this.quads[1] = new BlockQuad(basic_vertices[1], basic_texcoords, basic_lighting[1], upTexIndex, basic_indices);
		this.quads[2] = new BlockQuad(basic_vertices[2], basic_texcoords, basic_lighting[2], northTexIndex, basic_indices);
		this.quads[3] = new BlockQuad(basic_vertices[3], basic_texcoords, basic_lighting[3], southTexIndex, basic_indices);
		this.quads[4] = new BlockQuad(basic_vertices[4], basic_texcoords, basic_lighting[4], westTexIndex, basic_indices);
		this.quads[5] = new BlockQuad(basic_vertices[5], basic_texcoords, basic_lighting[5], eastTexIndex, basic_indices);
	}
	
	@Override
	public void addFaceQuadsToBuffer(Vector3i pos, Direction face, FloatBuffer vertices, FloatBuffer texCoords, FloatBuffer lighting, IntBuffer indices) {
		if (face != Direction.UNDEFINED) {
			int x = (pos.x % Chunk.width + Chunk.width) % Chunk.width;
			int y = (pos.y % Chunk.width + Chunk.width) % Chunk.width;
			int z = (pos.z % Chunk.width + Chunk.width) % Chunk.width;
			
			//this.quads[face.getIndex()].translate(x, y, z).addToBuffer(vertices, texCoords, lighting, indices);
			this.quads[face.getIndex()].addToBuffer(vertices, texCoords, lighting, indices, x, y, z);
		}
	}
	
}

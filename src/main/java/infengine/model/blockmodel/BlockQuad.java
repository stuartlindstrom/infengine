package main.java.infengine.model.blockmodel;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

import org.joml.Vector3f;

public class BlockQuad {
	
	private float[] vertices;
	private float[] texCoords;
	private float[] lighting;
	private int[] indices;
	//private Vector3f normal;
	
	public BlockQuad(float[] vertices, float[] texCoords, float[] lighting, int[] indices) {
		this.vertices = vertices;
		this.texCoords = texCoords;
		this.lighting = lighting;
		this.indices = indices;
	}
	
	public BlockQuad(float[] vertices, float[] texCoords, float[] lighting, int textureIndex, int[] indices) {
		this.vertices = vertices;
		this.texCoords = getTexCoords(texCoords, textureIndex);
		this.lighting = lighting;
		this.indices = indices;
	}
	
	public void addToBuffer(FloatBuffer vertices, FloatBuffer texCoords, FloatBuffer lighting, IntBuffer indices) {
		int vertexCount = vertices.position() / 3;
		for(int i = 0; i < this.vertices.length; i++) {
			if (vertices.position() < vertices.capacity()) {
				vertices.put(this.vertices[i]);
			}
		}
		for (int i = 0; i < this.texCoords.length; i++) {
			if (texCoords.position() < texCoords.capacity()) {
				texCoords.put(this.texCoords[i]);
			}
		}
		for (int i = 0; i < this.lighting.length; i++) {
			if (lighting.position() < lighting.capacity()) {
				lighting.put(this.lighting[i]);
			}
		}
		for (int i = 0; i < this.indices.length; i++) {
			if (indices.position() < indices.capacity()) {
				indices.put(this.indices[i] + vertexCount);
			}
		}
	}
	
	public void addToBuffer(FloatBuffer vertices, FloatBuffer texCoords, FloatBuffer lighting, IntBuffer indices, int xOffset, int yOffset, int zOffset) {
		int vertexCount = vertices.position() / 3;
		for(int i = 0; i < this.vertices.length; i++) {
			if (vertices.position() < vertices.capacity()) {
				float vert = this.vertices[i];
				if (i % 3 == 0) {
					vert += xOffset;
				} else if (i % 3 == 1) {
					vert += yOffset;
				} else if (i % 3 == 2) {
					vert += zOffset;
				}
				vertices.put(vert);
			}
		}
		for (int i = 0; i < this.texCoords.length; i++) {
			if (texCoords.position() < texCoords.capacity()) {
				texCoords.put(this.texCoords[i]);
			}
		}
		for (int i = 0; i < this.lighting.length; i++) {
			if (lighting.position() < lighting.capacity()) {
				lighting.put(this.lighting[i]);
			}
		}
		for (int i = 0; i < this.indices.length; i++) {
			if (indices.position() < indices.capacity()) {
				indices.put(this.indices[i] + vertexCount);
			}
		}
	}
	
	public BlockQuad translate(int xOffset, int yOffset, int zOffset) {
		float[] newVertices = new float[this.vertices.length];
		
		for (int i = 0; i < newVertices.length; i++) {
			if (i % 3 == 0) { // x
				newVertices[i] = this.vertices[i] + xOffset;
			} else if (i % 3 == 1) { // y
				newVertices[i] = this.vertices[i] + yOffset;
			} else { // z
				newVertices[i] = this.vertices[i] + zOffset;
			}
		}
		
		return new BlockQuad(newVertices, this.texCoords, this.lighting, this.indices);
	}
	
	/*
	 *  offsets and scales texCoords based on a texture index
	 */
	private static float[] getTexCoords(float[] texCoords, int index) {
		float[] offsetTexCoords = new float[texCoords.length];
		
		int xOffset = index % 16;
		int yOffset = index / 16;
		
		for (int i = 0; i < texCoords.length; i++) {
			offsetTexCoords[i] = (texCoords[i] / 16f) + (i % 2 == 0 ? xOffset : yOffset) / 16f;
		}
		
		return offsetTexCoords;
	}

}

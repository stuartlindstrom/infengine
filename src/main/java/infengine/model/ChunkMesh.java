package main.java.infengine.model;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.system.MemoryUtil;

import main.java.infengine.render.Texture;

public class ChunkMesh {
	
	public static Texture BLOCK_TEXTURES;
	static {
		try {
			BLOCK_TEXTURES = new Texture("src/main/resources/textures/blocks.png");
		} catch (Exception e) {
			System.out.println("WARNING: Failed to load texture atlas!");
		}
	}
	
	protected int vaoId = -1;
	protected final List<Integer> vboIds = new ArrayList<Integer>();
	protected int vertexCount;
	protected Texture texture;
	
	public ChunkMesh(FloatBuffer vertexBuffer, FloatBuffer textureBuffer, FloatBuffer lightBuffer, IntBuffer indexBuffer, int vertexCount) {
		this.texture = BLOCK_TEXTURES;
		try {
			if (vertexCount > 0) {
				vertexBuffer.flip();
				textureBuffer.flip();
				lightBuffer.flip();
				indexBuffer.flip();
				
				this.vertexCount = vertexCount; 
				
				// Create the vao and bind opengl to it
				this.vaoId = glGenVertexArrays();
				glBindVertexArray(this.vaoId);
			
				int vboId;
				
				// Creates the vertex vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ARRAY_BUFFER, vboId);
				glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				
				// Creates the texture vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ARRAY_BUFFER, vboId);
				glBufferData(GL_ARRAY_BUFFER, textureBuffer, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
				
				// Creates the lighting vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ARRAY_BUFFER, vboId);
				glBufferData(GL_ARRAY_BUFFER, lightBuffer, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);
				
				// Creates the index vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL_DYNAMIC_DRAW);
			
				// Unbind the vao and vbo objects from opengl
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);
			} else {
				this.vertexCount = 0;
				this.vaoId = -1;
			}
		} finally {
			if (vertexBuffer != null) {
				MemoryUtil.memFree(vertexBuffer);
			}
			if (textureBuffer != null) {
				MemoryUtil.memFree(textureBuffer);
			}
			if (lightBuffer != null) {
				MemoryUtil.memFree(lightBuffer);
			}
			if (indexBuffer != null) {
				MemoryUtil.memFree(indexBuffer);
			}
		}
	}
	
	public void updateMesh(FloatBuffer vertexBuffer, FloatBuffer textureBuffer, FloatBuffer lightBuffer, IntBuffer indexBuffer, int vertexCount) {
		try {
			vertexBuffer.flip();
			textureBuffer.flip();
			lightBuffer.flip();
			indexBuffer.flip();
			
			this.vertexCount = vertexCount; 
			
			if (this.vboIds.size() == 0 && this.vaoId == -1) { // if the chunk used to be empty
				// Create and bind the vao
				this.vaoId = glGenVertexArrays();
				glBindVertexArray(this.vaoId);
			
				int vboId;
				
				// Creates the vertex vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ARRAY_BUFFER, vboId);
				glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				
				// Creates the texture vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ARRAY_BUFFER, vboId);
				glBufferData(GL_ARRAY_BUFFER, textureBuffer, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
				
				// Creates the lighting vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ARRAY_BUFFER, vboId);
				glBufferData(GL_ARRAY_BUFFER, lightBuffer, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);
				
				// Creates the index vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL_DYNAMIC_DRAW);
			} else {
				// bind opengl to the vao
				glBindVertexArray(this.vaoId);
				
				// Binds the vertex vbo
				glBindBuffer(GL_ARRAY_BUFFER, this.vboIds.get(0));
				//glMapBufferRange(GL_ARRAY_BUFFER, 0, 1, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
				glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_DYNAMIC_DRAW);
				
				// Binds the texture vbo
				glBindBuffer(GL_ARRAY_BUFFER, this.vboIds.get(1));
				//glMapBufferRange(GL_ARRAY_BUFFER, 0, 1, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
				glBufferData(GL_ARRAY_BUFFER, textureBuffer, GL_DYNAMIC_DRAW);
				
				// Binds the lighting vbo
				glBindBuffer(GL_ARRAY_BUFFER, this.vboIds.get(2));
				//glMapBufferRange(GL_ARRAY_BUFFER, 0, 1, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
				glBufferData(GL_ARRAY_BUFFER, lightBuffer, GL_DYNAMIC_DRAW);
				
				// Binds the index vbo
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.vboIds.get(3));
				//glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, 1, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL_DYNAMIC_DRAW);
			}
		
			// Unbind the vao and vbo objects from opengl
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		} finally {
			if (vertexBuffer != null) {
				MemoryUtil.memFree(vertexBuffer);
			}
			if (textureBuffer != null) {
				MemoryUtil.memFree(textureBuffer);
			}
			if (lightBuffer != null) {
				MemoryUtil.memFree(lightBuffer);
			}
			if (indexBuffer != null) {
				MemoryUtil.memFree(indexBuffer);
			}
		}
	}
	
	public boolean isEmpty() {
		return this.vertexCount == 0;
	}
	
	public void render() {
		glActiveTexture(GL_TEXTURE0);
		texture.bind();
		
		glBindVertexArray(this.vaoId);
		
		// Enable the coordinate vbo
		glEnableVertexAttribArray(0);
		// Enable the texture vbo
		glEnableVertexAttribArray(1);
		// Enable the lighting vbo
		glEnableVertexAttribArray(2);
		
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		//glDisable(GL_CULL_FACE);
		glDrawElements(GL_TRIANGLES, this.vertexCount, GL_UNSIGNED_INT, 0);
		
		// Disable the coordinate vbo
		glDisableVertexAttribArray(0);
		// Disable the texture vbo
		glDisableVertexAttribArray(1);
		// Disable the lighting vbo
		glDisableVertexAttribArray(2);
		
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public void cleanup() {
		glDisableVertexAttribArray(0);
		
		// Delete the vbos
		for (int vboId : this.vboIds) {
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glDeleteBuffers(vboId);
		}
		
		// Delete the vao
		glBindVertexArray(0);
        glDeleteVertexArrays(vaoId);
	}

}

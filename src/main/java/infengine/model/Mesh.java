package main.java.infengine.model;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.system.MemoryUtil;

import main.java.infengine.render.Texture;

public class Mesh {
	
	protected final int vaoId;
	protected final List<Integer> vboIds = new ArrayList<Integer>();
	
	protected final int vertexCount;
	
	protected Texture texture;
	
	public Mesh(float[] vertices, float[] colors, float[] texCoords, int[] indices, Texture texture) {
		this.texture = texture;
		FloatBuffer vertexBuffer = null;
		// FloatBuffer colorBuffer = null;
		FloatBuffer textureBuffer = null;
		IntBuffer indexBuffer = null;
		try {
			vertexBuffer = MemoryUtil.memAllocFloat(vertices.length);
			vertexBuffer.put(vertices).flip();
			
			// colorBuffer = MemoryUtil.memAllocFloat(colors.length);
			// colorBuffer.put(colors).flip();
			
			textureBuffer = MemoryUtil.memAllocFloat(texCoords.length);
			textureBuffer.put(texCoords).flip();
			
			indexBuffer = MemoryUtil.memAllocInt(indices.length);
			this.vertexCount = indices.length; 
			indexBuffer.put(indices).flip();
		
			// Create the vao and bind opengl to it
			this.vaoId = glGenVertexArrays();
			glBindVertexArray(this.vaoId);
		
			int vboId;
			
			// Creates the vertex vbo
			vboId = glGenBuffers();
			this.vboIds.add(vboId);
			glBindBuffer(GL_ARRAY_BUFFER, vboId);
			glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			
			// Creates the color vbo
			// vboId = glGenBuffers();
			// this.vboIds.add(vboId);
			// glBindBuffer(GL_ARRAY_BUFFER, vboId);
			// glBufferData(GL_ARRAY_BUFFER, colorBuffer, GL_STATIC_DRAW);
			// glVertexAttribPointer(1, 3, GL_FLOAT, false, 0, 0);
			
			// Creates the texture vbo
			vboId = glGenBuffers();
			this.vboIds.add(vboId);
			glBindBuffer(GL_ARRAY_BUFFER, vboId);
			glBufferData(GL_ARRAY_BUFFER, textureBuffer, GL_STATIC_DRAW);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
			
			// Creates the index vbo
			vboId = glGenBuffers();
			this.vboIds.add(vboId);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL_STATIC_DRAW);
		
			// Unbind the vao and vbo objects from opengl
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		} finally {
			if (vertexBuffer != null) {
				MemoryUtil.memFree(vertexBuffer);
			}
			//if (colorBuffer != null) {
			//	MemoryUtil.memFree(colorBuffer);
			//}
			if (textureBuffer != null) {
				MemoryUtil.memFree(textureBuffer);
			}
			if (indexBuffer != null) {
				MemoryUtil.memFree(indexBuffer);
			}
		}
	}
	
	public void render() {
		glActiveTexture(GL_TEXTURE0);
		texture.bind();
		
		glBindVertexArray(this.vaoId);
		
		// Enable the coordinate vbo
		glEnableVertexAttribArray(0);
		// Enable the color vbo
		//glEnableVertexAttribArray(1);
		// Enable the texture vbo
		glEnableVertexAttribArray(1);
		
		glDrawElements(GL_TRIANGLES, this.vertexCount, GL_UNSIGNED_INT, 0);
		
		// Disable the coordinate vbo
		glDisableVertexAttribArray(0);
		// Disable the color vbo
		//glDisableVertexAttribArray(1);
		// Disable the texture vbo
		glDisableVertexAttribArray(1);
		
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public void cleanup() {
		this.texture.cleanup();
		
		glDisableVertexAttribArray(0);
		
		// Delete the vbos
		for (int vboId : this.vboIds) {
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glDeleteBuffers(vboId);
		}
		
		// Delete the vao
		glBindVertexArray(0);
        glDeleteVertexArrays(vaoId);
	}

}

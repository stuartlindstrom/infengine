package main.java.infengine;

public class Main {

	public static void main(String[] args) {
		try {
			InfEngine.infEngine.start();
		} catch (Exception excp) {
			excp.printStackTrace();
			System.exit(-1);
		}
	}

}

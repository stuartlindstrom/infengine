package main.java.infengine.input;

import static org.lwjgl.glfw.GLFW.*;

import org.joml.Vector2d;
import org.lwjgl.glfw.GLFWCursorEnterCallback;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;

import main.java.infengine.render.Window;

public class MouseInput {
	
	private Vector2d pos = new Vector2d(0, 0);
	private Vector2d prevPos = new Vector2d(0, 0);
	private boolean inWindow = false;
	private boolean leftPressed = false;
	private boolean rightPressed = false;
	private boolean middlePressed = false;
	private double scrollOffset = 0;
    private GLFWCursorPosCallback cursorPosCallback;
    private GLFWCursorEnterCallback cursorEnterCallback;
    private GLFWMouseButtonCallback mouseButtonCallback;
    private GLFWScrollCallback scrollCallback;
    
    public void init(Window window) {
    	glfwSetInputMode(window.getWindowHandle(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    	
		glfwSetCursorPosCallback(window.getWindowHandle(), cursorPosCallback = new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double xpos, double ypos) {
            	pos.x = xpos;
                pos.y = ypos;
            }
        });
        glfwSetCursorEnterCallback(window.getWindowHandle(), cursorEnterCallback = new GLFWCursorEnterCallback() {
            @Override
            public void invoke(long window, boolean entered) {
            	inWindow = entered;
            }
        });
        glfwSetMouseButtonCallback(window.getWindowHandle(), mouseButtonCallback = new GLFWMouseButtonCallback() {
            @Override
            public void invoke(long window, int button, int action, int mods) {
            	leftPressed = button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS;
                rightPressed = button == GLFW_MOUSE_BUTTON_2 && action == GLFW_PRESS;
                middlePressed = button == GLFW_MOUSE_BUTTON_3 && action == GLFW_PRESS;
            }
        });
        glfwSetScrollCallback(window.getWindowHandle(), scrollCallback = new GLFWScrollCallback() {
			@Override
			public void invoke(long window, double xOffset, double yOffset) {
				scrollOffset += yOffset;
			}
        });
	}
    
    public void update(Window window) {
    	prevPos.x = pos.x;
    	prevPos.y = pos.y;
    }
    
    public boolean inWindow() {
		return this.inWindow;
	}
	
	public Vector2d getPos() {
		return this.pos;
	}
	
	public Vector2d getPrevPos() {
		return this.prevPos;
	}
	
	public boolean rightButtonPressed() {
		return this.rightPressed;
	}
	
	public boolean leftButtonPressed() {
		return this.leftPressed;
	}
	
	public boolean middleButtonPressed() {
		return this.middlePressed;
	}
	
	public double getScrollOffset() {
		double offset = this.scrollOffset;
		this.scrollOffset = 0;
		return offset;
	}

}

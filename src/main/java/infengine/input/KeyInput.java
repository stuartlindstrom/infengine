package main.java.infengine.input;

import static org.lwjgl.glfw.GLFW.*;

import org.lwjgl.glfw.GLFWKeyCallback;

import main.java.infengine.InfEngine;
import main.java.infengine.render.Window;

public class KeyInput {
	
	private boolean wPressed = false;
	private boolean aPressed = false;
	private boolean sPressed = false;
	private boolean dPressed = false;
	private boolean shiftPressed = false;
	private boolean spacePressed = false;
	
	private boolean[] pressedKeys = new boolean[GLFW_KEY_LAST - GLFW_KEY_SPACE];
	private static final int keyArrayOffset = GLFW_KEY_SPACE;
	
	public void init(Window window) {
		glfwSetKeyCallback(window.getWindowHandle(), new GLFWKeyCallback() {
			@Override
			public void invoke(long window, int key, int scancode, int action, int mods) {
				if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
					glfwSetWindowShouldClose(window, true);
				} else if (key == GLFW_KEY_E && action == GLFW_RELEASE) {
					InfEngine.freeMouse = !InfEngine.freeMouse;
					if (InfEngine.freeMouse) {
						glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
					} else {
						glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
					}
				} else if (key == GLFW_KEY_F && action == GLFW_RELEASE) {
					InfEngine.infEngine.player.setHasGravity(!InfEngine.infEngine.player.hasGravity());
				} else if (key == GLFW_KEY_C && action == GLFW_RELEASE) {
					InfEngine.infEngine.player.setNoClip(!InfEngine.infEngine.player.noClip());
				}
				
				if (action == GLFW_PRESS && key - keyArrayOffset >= 0) {
					KeyInput.this.pressedKeys[key - keyArrayOffset] = true;
				} else if (action == GLFW_RELEASE && key - keyArrayOffset >= 0) {
					KeyInput.this.pressedKeys[key - keyArrayOffset] = false;
				}
				
				if (key == GLFW_KEY_W) {
					if (action == GLFW_PRESS) {
						wPressed = true;
					} else if (action == GLFW_RELEASE) {
						wPressed = false;
					}
				} else if (key == GLFW_KEY_A) {
					if (action == GLFW_PRESS) {
						aPressed = true;
					} else if (action == GLFW_RELEASE) {
						aPressed = false;
					}
				} else if (key == GLFW_KEY_S) {
					if (action == GLFW_PRESS) {
						sPressed = true;
					} else if (action == GLFW_RELEASE) {
						sPressed = false;
					}
				} else if (key == GLFW_KEY_D) {
					if (action == GLFW_PRESS) {
						dPressed = true;
					} else if (action == GLFW_RELEASE) {
						dPressed = false;
					}
				} else if (key == GLFW_KEY_LEFT_SHIFT) {
					if (action == GLFW_PRESS) {
						shiftPressed = true;
					} else if (action == GLFW_RELEASE) {
						shiftPressed = false;
					}
				} else if (key == GLFW_KEY_SPACE) {
					if (action == GLFW_PRESS) {
						spacePressed = true;
					} else if (action == GLFW_RELEASE) {
						spacePressed = false;
					}
				}
			}
		});
	}
	
	public boolean isKeyPressed(Window window, int keyCode) {
		return glfwGetKey(window.getWindowHandle(), keyCode) == GLFW_PRESS;
	}
	
	public boolean isKeyPressed(int glfwKeyCode, boolean resetKey) {
		if (glfwKeyCode - keyArrayOffset >= 0 && glfwKeyCode - keyArrayOffset < this.pressedKeys.length) {
			boolean ret = this.pressedKeys[glfwKeyCode - keyArrayOffset];
			if (resetKey) this.pressedKeys[glfwKeyCode - keyArrayOffset] = false;
			return ret;
		}
		return false;
	}
	
	public boolean isKeyPressed(int glfwKeyCode) {
		return this.isKeyPressed(glfwKeyCode, false);
	}
	
	public boolean wPressed() {
		return this.wPressed;
	}
	
	public boolean aPressed() {
		return this.aPressed;
	}
	
	public boolean sPressed() {
		return this.sPressed;
	}
	
	public boolean dPressed() {
		return this.dPressed;
	}
	
	public boolean shiftPressed() {
		return this.shiftPressed;
	}
	
	public boolean spacePressed() {
		return this.spacePressed;
	}

}

package main.java.infengine.entity;

import main.java.infengine.block.Block;
import main.java.infengine.block.Blocks;
import main.java.infengine.util.AxisAlignedBB;
import main.java.infengine.world.World;

public class Entity {
	
	public World world;
	
	public double x, y, z;
	public double prevX, prevY, prevZ;
	public double xVel, yVel, zVel;
	public double maxVelocity = 0.98;
	
	public double roll, yaw, pitch;
	public double prevRoll, prevYaw, prevPitch;
	
	public double scale;
	
	protected float height = 0.5f;
	protected float width = 0.5f;
	
	protected final AxisAlignedBB collisionAABB;
	protected final AxisAlignedBB renderAABB;
	
	protected boolean hasGravity = true;
	protected boolean noClip = false;
	
	public boolean onGround = false;
	
	protected int ticksExisted;
	
	private int entityID;
	
	private static int nextID = 0;
	
	public Entity(World world) {
		this.world = world;
		this.scale = 1;
		
		this.collisionAABB = new AxisAlignedBB(0, 0, 0, 0, 0, 0);
		this.renderAABB = new AxisAlignedBB(0, 0, 0, 0, 0, 0);
		
		this.ticksExisted = 0;
		
		this.entityID = nextID++;
	}
	
	public void update() {
		if (hasGravity) {
			yVel -= 0.06;
		}
		
		xVel = Math.min(xVel, maxVelocity);
		yVel = Math.min(yVel, maxVelocity);
		zVel = Math.min(zVel, maxVelocity);
		
		tryMove(xVel, yVel, zVel);
		
		this.prevX = this.x;
		this.prevY = this.y;
		this.prevZ = this.y;
		
		this.prevRoll = roll;
		this.prevYaw = yaw;
		this.prevPitch = pitch;
		
		this.ticksExisted++;
	}
	
	private AxisAlignedBB tempAABB = new AxisAlignedBB(0, 0, 0, 0, 0, 0);
	private AxisAlignedBB nextAABB = new AxisAlignedBB(0, 0, 0, 0, 0, 0);
	public void tryMove(double velX, double velY, double velZ) {
		double dX = velX, dY = velY, dZ = velZ;
		
		if (!noClip) {
			AxisAlignedBB currentAABB = getCollisionAABB();
			nextAABB.setFromTranslated(currentAABB, 0, 0, 0);
			
			int minX = (int) Math.floor(currentAABB.minX + Math.min(0, velX) - 1);
			int minY = (int) Math.floor(currentAABB.minY + Math.min(0, velY) - 1);
			int minZ = (int) Math.floor(currentAABB.minZ + Math.min(0, velZ) - 1);
			int maxX = (int) Math.floor(currentAABB.maxX + Math.max(0, velX) + 1);
			int maxY = (int) Math.floor(currentAABB.maxY + Math.max(0, velY) + 1);
			int maxZ = (int) Math.floor(currentAABB.maxZ + Math.max(0, velZ) + 1);
			
			if (dY != 0) {
				for (int x = minX; x < maxX; x++) {
					for (int y = minY; y < maxY; y++) {
						for (int z = minZ; z < maxZ; z++) {
							Block block = Blocks.BLOCKS[world.getBlockID(x, y, z)];
							tempAABB.setFromTranslated(block.getCollisionAABB(), x, y, z);
							dY = tempAABB.calcYOffset(nextAABB, dY);
						}
					}
				}
				nextAABB.translate(0, dY, 0);
			}
			
			boolean xFirst = Math.abs(dX) > Math.abs(dZ);
			if (xFirst) {
				if (dX != 0) {
					for (int x = minX; x < maxX; x++) {
						for (int y = minY; y < maxY; y++) {
							for (int z = minZ; z < maxZ; z++) {
								Block block = Blocks.BLOCKS[world.getBlockID(x, y, z)];
								tempAABB.setFromTranslated(block.getCollisionAABB(), x, y, z);
								dX = tempAABB.calcXOffset(nextAABB, dX);
							}
						}
					}
					nextAABB.translate(dX, 0, 0);
				}
			}
			if (dZ != 0) {
				for (int x = minX; x < maxX; x++) {
					for (int y = minY; y < maxY; y++) {
						for (int z = minZ; z < maxZ; z++) {
							Block block = Blocks.BLOCKS[world.getBlockID(x, y, z)];
							tempAABB.setFromTranslated(block.getCollisionAABB(), x, y, z);
							dZ = tempAABB.calcZOffset(nextAABB, dZ);
						}
					}
				}
				nextAABB.translate(0, 0, dZ);
			}
			if (!xFirst) {
				if (dX != 0) {
					for (int x = minX; x < maxX; x++) {
						for (int y = minY; y < maxY; y++) {
							for (int z = minZ; z < maxZ; z++) {
								Block block = Blocks.BLOCKS[world.getBlockID(x, y, z)];
								tempAABB.setFromTranslated(block.getCollisionAABB(), x, y, z);
								dX = tempAABB.calcXOffset(nextAABB, dX);
							}
						}
					}
					nextAABB.translate(dX, 0, 0);
				}
			}
		}
		
		this.onGround = dY != velY && velY < 0;
		
		if (dY != velY) this.yVel = 0;
		if (dX != velX) this.xVel = 0;
		if (dZ != velZ) this.zVel = 0;
		
		this.x += dX;
		this.y += dY;
		this.z += dZ;
	}
	
	public AxisAlignedBB getCollisionAABB() {
		return this.collisionAABB.translated(x, y, z);
	}
	
	public AxisAlignedBB getRenderAABB() {
		return this.renderAABB.translated(x, y, z);
	}
	
	public int getTicksExisted() {
		return this.ticksExisted;
	}
	
	public int getID() {
		return this.entityID;
	}
	
	public boolean hasGravity() {
		return this.hasGravity;
	}
	
	public void setHasGravity(boolean gravity) {
		this.hasGravity = gravity;
	}
	
	public boolean noClip() {
		return this.noClip;
	}
	
	public void setNoClip(boolean noClip) {
		this.noClip = noClip;
	}
	
	public double distanceSquared(double x, double y, double z) {
		double dX = this.x - x;
		double dY = this.y - y;
		double dZ = this.z - z;
		return ((dX * dX) + (dY * dY) + (dZ * dZ));
	}
	
	public double distance(double x, double y, double z) {
		return Math.sqrt(distanceSquared(x, y, z));
	}
	
	public double distanceSquared(Entity entity) {
		return distanceSquared(entity.x, entity.y, entity.z);
	}
	
	public double distance(Entity entity) {
		return Math.sqrt(distanceSquared(entity));
	}
    
}

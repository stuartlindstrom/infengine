package main.java.infengine.entity.mob.player;

import org.lwjgl.glfw.GLFW;

import main.java.infengine.InfEngine;
import main.java.infengine.entity.mob.EntityMob;
import main.java.infengine.world.World;

public class EntityPlayer extends EntityMob {

	protected float accelerationX, accelerationY, accelerationZ;
	protected float flySpeed = 0.15f;
	
	public EntityPlayer(World world) {
		super(world);
		this.height = 1.8f;
		this.width = 0.6f;
		this.eyeHeight = this.height * 0.875f;
		
		this.moveSpeed = 0.08f;
		
		this.collisionAABB.setBounds(-width / 2, 0, -width / 2, width / 2, height, width / 2);
		this.renderAABB.setBounds(-width / 2, 0, -width / 2, width / 2, height, width / 2);
	}
	
	@Override
	public void update() {
		xVel += accelerationX;
		yVel += accelerationY;
		zVel += accelerationZ;
		
		if (hasGravity) {
			yVel -= 0.06;
		}
		
		xVel = Math.min(xVel, maxVelocity);
		yVel = Math.min(yVel, maxVelocity);
		zVel = Math.min(zVel, maxVelocity);
		
		tryMove(xVel, yVel, zVel);
		
		if (!this.hasGravity) this.yVel *= 0.7f;
		this.xVel *= 0.7f;
		this.zVel *= 0.7f;
		
		accelerationX = 0;
		accelerationY = 0;
		accelerationZ = 0;
		
		this.prevX = this.x;
		this.prevY = this.y;
		this.prevZ = this.z;
		
		if (this.pitch < -90) this.pitch = -90;
		if (this.pitch > 90) this.pitch = 90;
		if (this.headPitch < -90) this.headPitch = -90;
		if (this.headPitch > 90) this.headPitch = 90;
		
		this.yaw = this.yaw % 360;
		this.roll = this.roll % 360;
		this.headYaw = this.headYaw % 360;
		this.headRoll = this.headRoll % 360;
		
		this.prevRoll = roll;
		this.prevYaw = yaw;
		this.prevPitch = pitch;
		
		this.prevHeadRoll = headRoll;
		this.prevHeadYaw = headYaw;
		this.prevHeadPitch = headPitch;
		
		this.ticksExisted++;
	}
	
	public void jump() {
		if (this.onGround) {
			this.yVel += 0.4;
			this.onGround = false;
		}
	}
	
	public void move(float dX, float dY, float dZ) {
		float speed = (this.hasGravity && !this.onGround) ? this.moveSpeed * 0.6f : (!this.hasGravity) ? this.flySpeed : this.moveSpeed;
		if (InfEngine.infEngine.keyInput.isKeyPressed(GLFW.GLFW_KEY_LEFT_SHIFT) && this.hasGravity) speed *= 0.5f;
		
        if (dZ != 0) {
        	accelerationX = (float)Math.sin(Math.toRadians(yaw + headYaw)) * -dZ * speed;
        	accelerationZ = (float)Math.cos(Math.toRadians(yaw + headYaw)) * dZ * speed;
        }
        if (dX != 0) {
        	if (dZ != 0) {
        		accelerationX += (float)Math.sin(Math.toRadians(yaw + headYaw - 90)) * -dX * speed;
        		accelerationZ += (float)Math.cos(Math.toRadians(yaw + headYaw - 90)) * dX * speed;
        	} else {
        		accelerationX = (float)Math.sin(Math.toRadians(yaw + headYaw - 90)) * -dX * speed;
        		accelerationZ = (float)Math.cos(Math.toRadians(yaw + headYaw - 90)) * dX * speed;
        	}
        }
        if (dY != 0) {
        	accelerationY = dY * speed;
        }
    }

}

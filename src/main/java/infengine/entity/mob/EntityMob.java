package main.java.infengine.entity.mob;

import main.java.infengine.entity.Entity;
import main.java.infengine.world.World;

public class EntityMob extends Entity {

	protected float eyeHeight = 1.2f * 0.875f;
	
	public double headRoll, headYaw, headPitch;
	public double prevHeadRoll, prevHeadYaw, prevHeadPitch;
	
	protected float moveSpeed = 0.2f;
	
	public EntityMob(World world) {
		super(world);
	}
	
	@Override
	public void update() {
		this.prevHeadRoll = headRoll;
		this.prevHeadYaw = headYaw;
		this.prevHeadPitch = headPitch;
		
		super.update();
	}
	
	public float getEyeHeight() {
		return this.eyeHeight;
	}

}

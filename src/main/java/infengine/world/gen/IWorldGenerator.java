package main.java.infengine.world.gen;

import main.java.infengine.world.chunk.Chunk;
import main.java.infengine.world.chunk.ChunkPos;
import main.java.infengine.world.chunk.IChunkPos;

public interface IWorldGenerator {
	
	public void generateTerrain(Chunk chunk, IChunkPos chunkPos, long seed);
	
	public int terrainHeight(int x, int z, long seed);

}

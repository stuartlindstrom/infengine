package main.java.infengine.world.gen;

import main.java.infengine.block.Blocks;
import main.java.infengine.util.NoiseUtils;
import main.java.infengine.world.chunk.Chunk;
import main.java.infengine.world.chunk.IChunkPos;

public class BasicWorldGenerator implements IWorldGenerator {
	
	/*
	@Override
	public void generateTerrain(Chunk chunk, IChunkPos chunkPos, long seed) {
		for (int x = 0; x < Chunk.width; x++) {
			int blockX = chunkPos.getBlockX() + x;
			for (int z = 0; z < Chunk.width; z++) {
				int blockZ = chunkPos.getBlockZ() + z;
				
				int terrainHeight = terrainHeight(blockX, blockZ, seed);
				
				int prevTurb = 0;
				int prevPrevTurb = 0;
				if (chunkPos.getBlockY() - terrainHeight <= 30 && chunkPos.getBlockY() + Chunk.width - terrainHeight >= -30) {
					prevTurb = turbulence(blockX, chunkPos.getBlockY() + Chunk.width, blockZ, seed);
					prevPrevTurb = turbulence(blockX, chunkPos.getBlockY() + Chunk.width + 1, blockZ, seed);
				}
				for (int y = Chunk.width - 1; y >= 0; y--) {
					int blockY = chunkPos.getBlockY() + y;
					
					int turb = (blockY  - terrainHeight <= 30 && blockY - terrainHeight >= -30) ? turbulence(blockX, blockY, blockZ, seed) : 0;
					//int turb = 0, prevTurb = 0, prevPrevTurb = 0;
					
					if (blockY < terrainHeight + turb) {
						if (blockY + 1 >= terrainHeight + prevTurb) {
							chunk.setBlock(Blocks.GRASS, x, y, z, false);
						} else if (blockY + 2 >= terrainHeight + prevPrevTurb - 1) {
							chunk.setBlock(Blocks.DIRT, x, y, z, false);
						} else {
							chunk.setBlock(Blocks.STONE, x, y, z, false);
						}
					} else {
						chunk.setBlock(Blocks.AIR, x, y, z, false);
					}
					prevPrevTurb = prevTurb;
					prevTurb = turb;
				}
			}
		}
	}
	*/
	
	@Override
	public void generateTerrain(Chunk chunk, IChunkPos chunkPos, long seed) {
		int chunkBlockX = chunkPos.getBlockX();
		int chunkBlockY = chunkPos.getBlockY();
		int chunkBlockZ = chunkPos.getBlockZ();
		
		int[][][] turbulence = new int[3][4][3];
		for (int z = 0; z < 3; z++) {
			for (int y = 0; y < 3; y++) {
				for (int x = 0; x < 3; x++) {
					turbulence[z][y][x] = turbulence(chunkBlockX + (x * 8), chunkBlockY + (y * 8), chunkBlockZ + (z * 8), seed);
				}
			}
		}
		
		
		for (int x = 0; x < Chunk.width; x++) {
			int blockX = chunkBlockX + x;
			for (int z = 0; z < Chunk.width; z++) {
				int blockZ = chunkBlockZ + z;
				
				int terrainHeight = terrainHeight(blockX, blockZ, seed);
				
				int prevTurb = interpolatedTurb(x, Chunk.width, z, turbulence);
				int prevPrevTurb = interpolatedTurb(x, Chunk.width + 1, z, turbulence);
				for (int y = Chunk.width - 1; y >= 0; y--) {
					int blockY = chunkBlockY + y;
					
					int turb = interpolatedTurb(x, y, z, turbulence);
					
					if (blockY < terrainHeight + turb) {
						if (blockY + 1 >= terrainHeight + prevTurb) {
							chunk.setBlock(Blocks.GRASS, x, y, z, false);
						} else if (blockY + 2 >= terrainHeight + prevPrevTurb - 1) {
							chunk.setBlock(Blocks.DIRT, x, y, z, false);
						} else {
							chunk.setBlock(Blocks.STONE, x, y, z, false);
						}
					} else {
						chunk.setBlock(Blocks.AIR, x, y, z, false);
					}
					prevPrevTurb = prevTurb;
					prevTurb = turb;
				}
			}
		}
	}
	
	private int interpolatedTurb(int x, int y, int z, int[][][] vals) {
		float u = (x % 8) / 8f;
		float v = (y % 8) / 8f;
		float w = (z % 8) / 8f;
		
		int x1 = (int) (x / 8);
		int x2 = (int) (x / 8) + 1;
		int y1 = (int) (y / 8);
		int y2 = (int) (y / 8) + 1;
		int z1 = (int) (z / 8);
		int z2 = (int) (z / 8) + 1;
		
		// z(y0(x0, x1, y0), y1(x0, x1, y1), z)
		return lerp(lerp(lerp(vals[z1][y1][x1], vals[z1][y1][x2], fade(u)), lerp(vals[z1][y2][x1], vals[z1][y2][x2], fade(u)), fade(v)), lerp(lerp(vals[z2][y1][x1], vals[z2][y1][x2], fade(u)), lerp(vals[z2][y2][x1], vals[z2][y2][x2], fade(u)), fade(v)), fade(w));
	}
	
	private int lerp(int a, int b, float x) {
		return (int) (a + x * (b - a));
	}

	private float fade(float d) {
		return d * d * d * ((d * ((d * 6) - 15)) + 10);
	}
	
	@Override
	public int terrainHeight(int x, int z, long seed) {
		return (int) (1024 * Math.pow(NoiseUtils.getNoise(x, z, seed, 213, 1, 0.5, 5), 0.5)) - 512;
	}
	
	private int turbulence(int x, int y, int z, long seed) {
		return (int) (128 * NoiseUtils.getNoise(x, y, z, seed, 35, 1, 0.4, 1)) - 64;
	}

}

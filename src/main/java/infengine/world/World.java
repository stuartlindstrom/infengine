package main.java.infengine.world;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.joml.Intersectiond;

import main.java.infengine.InfEngine;
import main.java.infengine.block.Blocks;
import main.java.infengine.entity.Entity;
import main.java.infengine.entity.mob.player.EntityPlayer;
import main.java.infengine.util.Direction;
import main.java.infengine.world.chunk.Chunk;
import main.java.infengine.world.chunk.ChunkPos;
import main.java.infengine.world.chunk.IChunkPos;
import main.java.infengine.world.chunk.MutableChunkPos;
import main.java.infengine.world.gen.BasicWorldGenerator;
import main.java.infengine.world.gen.FloatingIslandGenerator;
import main.java.infengine.world.gen.IWorldGenerator;

public class World {

	public Map<ChunkPos, Chunk> loadedChunks = new HashMap<ChunkPos, Chunk>();
	public Map<Integer, Entity> loadedEntities = new HashMap<Integer, Entity>();
	public Random rand;
	private long seed;
	private int ticks;
	private IWorldGenerator generator;

	public World() {
		this.seed = System.currentTimeMillis();
		this.rand = new Random(this.seed);
		this.ticks = 0;
		this.generator = new BasicWorldGenerator();
	}

	public long getSeed() {
		return this.seed;
	}
	
	public IWorldGenerator getGenerator() {
		return this.generator;
	}
	
	private boolean canLoadChunkAt(int x, int y, int z) { // 92 * 92 works well
		EntityPlayer player = InfEngine.infEngine.player;
		return Intersectiond.testAabSphere(x, y, z, x + Chunk.width, y + Chunk.width, z + Chunk.width, player.x, player.y + player.getEyeHeight(), player.z, /*10000*/16384);
	}
	
	private boolean canUnloadChunkAt(int x, int y, int z) {
		EntityPlayer player = InfEngine.infEngine.player;
		return !Intersectiond.testAabSphere(x, y, z, x + Chunk.width, y + Chunk.width, z + Chunk.width, player.x, player.y + player.getEyeHeight(), player.z, 25600);
	}

	private static int maxChunkLoad = 11;
	
	private List<ChunkPos> toLoad = new ArrayList<ChunkPos>();
	private MutableChunkPos tempCP = new MutableChunkPos(0, 0, 0);
	public void update() {
		int playerChunkX = ((int) InfEngine.infEngine.player.x) >> 4;
		int playerChunkY = ((int) InfEngine.infEngine.player.y) >> 4;
		int playerChunkZ = ((int) InfEngine.infEngine.player.z) >> 4;

		if (ticks % 1 == 0) {
			//long startTime = System.currentTimeMillis();
			
			int chunksLoaded = 0;
			
			if (!isChunkLoaded(playerChunkX, playerChunkY, playerChunkZ)) {
				loadChunk(new ChunkPos(playerChunkX, playerChunkY, playerChunkZ));
				chunksLoaded++;
			}
			
			Iterator<ChunkPos> chunkIter = this.loadedChunks.keySet().iterator();
			while(chunkIter.hasNext()) {
				if (chunksLoaded > maxChunkLoad) {
					break;
				}
				
				ChunkPos chunkPos = chunkIter.next();
				if (canUnloadChunkAt(chunkPos.getBlockX(), chunkPos.getBlockY(), chunkPos.getBlockZ())) {
					unLoadChunk(chunkPos);
					chunkIter.remove();
					continue;
				}
				
				for (int i = 0; i < 6; i++) {
					tempCP.setValues(chunkPos);
					tempCP.offset(Direction.fromIndex(i));
					if (!isChunkLoaded(tempCP) && canLoadChunkAt(tempCP.getBlockX(), tempCP.getBlockY(), tempCP.getBlockZ())) {
						toLoad.add(new ChunkPos(tempCP.getX(), tempCP.getY(), tempCP.getZ()));
						chunksLoaded++;
					}
				}
			}
			for (ChunkPos chunkPos : toLoad) {
				this.loadChunk(chunkPos);
			}
			toLoad.clear();
			
			//System.out.println("Chunk Management took: " + (System.currentTimeMillis() - startTime) + " millis");
		}
		
		Iterator<Entity> entityIter = this.loadedEntities.values().iterator();
		while (entityIter.hasNext()) {
			Entity entity = entityIter.next();
			
			if (entity != InfEngine.infEngine.player && entity.distance(InfEngine.infEngine.player) > 1024) {
				continue;
			}
			
			entity.update();
		}
		
		ticks++;
	}
	
	private MutableChunkPos tempCP0 = new MutableChunkPos(0, 0, 0);
	private void loadChunk(ChunkPos chunkPos) {
		Chunk chunk = new Chunk(this, chunkPos);
		this.loadedChunks.put(chunkPos, chunk);
		//chunk.generateTerrain();
		
		tempCP0.setValues(chunkPos);
		IChunkPos adj = tempCP0.offset(Direction.DOWN);
		if (loadedChunks.containsKey(adj)) {
			loadedChunks.get(adj).createMesh();
		}
		tempCP0.setValues(chunkPos);
		adj = tempCP0.offset(Direction.UP);
		if (loadedChunks.containsKey(adj)) {
			loadedChunks.get(adj).createMesh();
		}
		tempCP0.setValues(chunkPos);
		adj = tempCP0.offset(Direction.NORTH);
		if (loadedChunks.containsKey(adj)) {
			loadedChunks.get(adj).createMesh();
		}
		tempCP0.setValues(chunkPos);
		adj = tempCP0.offset(Direction.SOUTH);
		if (loadedChunks.containsKey(adj)) {
			loadedChunks.get(adj).createMesh();
		}
		tempCP0.setValues(chunkPos);
		adj = tempCP0.offset(Direction.WEST);
		if (loadedChunks.containsKey(adj)) {
			loadedChunks.get(adj).createMesh();
		}
		tempCP0.setValues(chunkPos);
		adj = tempCP0.offset(Direction.EAST);
		if (loadedChunks.containsKey(adj)) {
			loadedChunks.get(adj).createMesh();
		}
	}
	
	private void unLoadChunk(IChunkPos chunkPos) {
		Chunk chunk = this.loadedChunks.get(chunkPos);
		if (chunk != null) {
			for (int i = 0; i < 6; i++) {
				tempCP1.setValues(chunkPos);
				IChunkPos adj = tempCP1.offset(Direction.fromIndex(i));
				if (isChunkLoaded(adj)) {
					this.loadedChunks.get(adj).createMesh();
				}
			}
		}
		chunk.cleanup();
	}
	
	private MutableChunkPos tempCP1 = new MutableChunkPos(0, 0, 0);
	public boolean isChunkLoaded(int chunkX, int chunkY, int chunkZ) {
		tempCP1.setX(chunkX);
		tempCP1.setY(chunkY);
		tempCP1.setZ(chunkZ);
		return loadedChunks.containsKey(tempCP1);
	}
	
	public boolean isChunkLoaded(IChunkPos pos) {
		return loadedChunks.containsKey(pos);
	}
	
	private MutableChunkPos tempCP2 = new MutableChunkPos(0, 0, 0);
	public Chunk getChunk(int chunkX, int chunkY, int chunkZ) {
		tempCP2.setX(chunkX);
		tempCP2.setY(chunkY);
		tempCP2.setZ(chunkZ);
		return loadedChunks.get(tempCP2);
	}
	
	public Chunk getChunk(IChunkPos chunkPos) {
		return loadedChunks.get(chunkPos);
	}
	
	public void addEntity(Entity entity) {
		this.loadedEntities.put(entity.getID(), entity);
	}
	
	public int getTime() {
		return this.ticks;
	}
	
	private MutableChunkPos tempCP3 = new MutableChunkPos(0, 0, 0);
	public int getBlockID(int x, int y, int z) {
		tempCP3.setX(x >> 4);
		tempCP3.setY(y >> 4);
		tempCP3.setZ(z >> 4);
		if (this.loadedChunks.containsKey(tempCP3)) {
			return this.loadedChunks.get(tempCP3).getBlockID((x % Chunk.width + Chunk.width) % Chunk.width, (y % Chunk.width + Chunk.width) % Chunk.width, (z % Chunk.width + Chunk.width) % Chunk.width);
		}
		return Blocks.AIR;
	}
	
	public void setBlock(int blockID, int x, int y, int z, boolean rerender) {
		if (this.isChunkLoaded(x >> 4, y >> 4, z >> 4)) {
			Chunk chunk = this.getChunk(x >> 4, y >> 4, z >> 4);
			
			chunk.setBlock(blockID, (x % Chunk.width + Chunk.width) % Chunk.width, (y % Chunk.width + Chunk.width) % Chunk.width, (z % Chunk.width + Chunk.width) % Chunk.width, rerender);
		}
	}
	
	public int getTerrainHeight(int x, int z) {
		// TODO: take 3D noise into account
		return this.generator.terrainHeight(x, z, this.seed);
	}

	public void cleanup() {
		for (Chunk chunk : this.loadedChunks.values()) {
			chunk.cleanup();
		}
	}

}

package main.java.infengine.world.chunk;

import org.joml.Vector3i;

import main.java.infengine.util.Direction;

public class MutableChunkPos implements IChunkPos {
	
	private int x, y, z;
	private Vector3i blockCoord = new Vector3i(0, 0, 0);

	public MutableChunkPos(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public static MutableChunkPos fromBlockPos(int x, int y, int z) {
		return new MutableChunkPos(x >> 4, y >> 4, z >> 4);
	}
	
	@Override
	public int getX() {
		return this.x;
	}
	
	@Override
	public int getY() {
		return this.y;
	}
	
	@Override
	public int getZ() {
		return this.z;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setZ(int z) {
		this.z = z;
	}
	
	@Override
	public int getBlockX() {
		return (this.x) * Chunk.width;
	}
	
	@Override
	public int getBlockY() {
		return (this.y) * Chunk.width;
	}
	
	@Override
	public int getBlockZ() {
		return (this.z) * Chunk.width;
	}
	
	public IChunkPos offset(Direction dir) {
		if (dir == Direction.DOWN) {
			this.y -= 1;
		}
		if (dir == Direction.UP) {
			this.y += 1;
		}
		if (dir == Direction.NORTH) {
			this.z -= 1;
		}
		if (dir == Direction.SOUTH) {
			this.z += 1;
		}
		if (dir == Direction.WEST) {
			this.x -= 1;
		}
		if (dir == Direction.EAST) {
			this.x += 1;
		}
		return this;
	}
	
	public Vector3i getBlockCoord() {
		return this.blockCoord.set(getBlockX(), getBlockY(), getBlockZ());
	}
	
	public void setValues(IChunkPos cc) {
		this.x = cc.getX();
		this.y = cc.getY();
		this.z = cc.getZ();
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (other == null) {
			return false;
		}
		if (other instanceof IChunkPos) {
			IChunkPos cc = (IChunkPos) other;
			// TODO take this.world and other.world into account as well
			return cc.getX() == this.x && cc.getY() == this.y && cc.getZ() == this.z;
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		// TODO use a z-order curve
		int prime = 181;
		int prime2 = 19394489;
		int h = 1;
		h = prime * (h + this.x);
		h = prime * (h + this.y);
		h = prime * (h + this.z);
		return h;
	}

}

package main.java.infengine.world.chunk;

import org.joml.Vector3i;

import main.java.infengine.InfEngine;
import main.java.infengine.util.Direction;

public class ChunkPos implements IChunkPos {
	
	private final int x, y, z;
	private Vector3i blockCoord;
	
	public ChunkPos(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.blockCoord = new Vector3i(x * Chunk.width, y * Chunk.width, z * Chunk.width);
	}
	
	public static ChunkPos fromBlockPos(int x, int y, int z) {
		return new ChunkPos(x >> 4, y >> 4, z >> 4);
	}
	
	@Override
	public int getX() {
		return this.x;
	}
	
	@Override
	public int getY() {
		return this.y;
	}
	
	@Override
	public int getZ() {
		return this.z;
	}
	
	@Override
	public int getBlockX() {
		return (this.x) * Chunk.width;
	}
	
	@Override
	public int getBlockY() {
		return (this.y) * Chunk.width;
	}
	
	@Override
	public int getBlockZ() {
		return (this.z) * Chunk.width;
	}
	
	public ChunkPos offset(Direction dir) {
		if (dir == Direction.DOWN) {
			return new ChunkPos(x, y - 1, z);
		}
		if (dir == Direction.UP) {
			return new ChunkPos(x, y + 1, z);
		}
		if (dir == Direction.NORTH) {
			return new ChunkPos(x, y, z - 1);
		}
		if (dir == Direction.SOUTH) {
			return new ChunkPos(x, y, z + 1);
		}
		if (dir == Direction.WEST) {
			return new ChunkPos(x - 1, y, z);
		}
		if (dir == Direction.EAST) {
			return new ChunkPos(x + 1, y, z);
		}
		return new ChunkPos(x, y, z);
	}
	
	public Vector3i getBlockCoord() {
		return this.blockCoord;
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (other == null) {
			return false;
		}
		if (other instanceof IChunkPos) {
			IChunkPos cc = (IChunkPos) other;
			// TODO take this.world and other.world into account as well
			return cc.getX() == this.x && cc.getY() == this.y && cc.getZ() == this.z;
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		// TODO use a z-order curve
		int prime = 181;
		int prime2 = 19394489;
		int h = 1;
		h = prime * (h + this.x);
		h = prime * (h + this.y);
		h = prime * (h + this.z);
		return h;
	}

}

package main.java.infengine.world.chunk;

public interface IChunkPos {
	
	public int getX();
	public int getY();
	public int getZ();
	
	public int getBlockX();
	public int getBlockY();
	public int getBlockZ();

}

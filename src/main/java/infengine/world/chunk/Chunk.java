package main.java.infengine.world.chunk;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.joml.Vector3i;
import org.lwjgl.system.MemoryUtil;

import main.java.infengine.block.Block;
import main.java.infengine.block.Blocks;
import main.java.infengine.model.ChunkMesh;
import main.java.infengine.util.AxisAlignedBB;
import main.java.infengine.util.Direction;
import main.java.infengine.world.World;

public class Chunk {
	
	public static final int width = 16;
	public static final int layerSize = 256;
	public static final int size = 4096;
	
	private World world;
	private ChunkPos position;
	private int[] blocks;
	private ChunkMesh chunkMesh;
	private AxisAlignedBB boundingBox;
	
	public Chunk(World world, int x, int y, int z) {
		this.world = world;
		this.position = new ChunkPos(x, y, z);
		this.blocks = new int[this.size];
		int blockX = position.getBlockX(), blockY = position.getBlockY(), blockZ = position.getBlockZ();
		this.boundingBox = new AxisAlignedBB(blockX, blockY, blockZ, blockX + width, blockY + width, blockZ + width);
		world.getGenerator().generateTerrain(this, position, world.getSeed());
	}
	
	public Chunk(World world, IChunkPos position) {
		this.world = world;
		if (position instanceof ChunkPos) {
			this.position = (ChunkPos) position;
		} else {
			this.position = new ChunkPos(position.getX(), position.getY(), position.getZ());
		}
		this.blocks = new int[this.size];
		int blockX = position.getBlockX(), blockY = position.getBlockY(), blockZ = position.getBlockZ();
		this.boundingBox = new AxisAlignedBB(blockX, blockY, blockZ, blockX + width, blockY + width, blockZ + width);
		//double startTime = System.nanoTime() / 1_000_000.0;
		world.getGenerator().generateTerrain(this, position, world.getSeed());
		//System.out.println("Terrain Generation took: " + ((System.nanoTime() / 1_000_000.0) - startTime) + " millis");
	}
	
	public ChunkPos getPosition() {
		return this.position;
	}
	
	public AxisAlignedBB getAABB() {
		return boundingBox;
	}
	
	public int[] getBlocks() {
		return this.blocks;
	}
	
	public int getBlockID(int x, int y, int z) {
		return this.blocks[(z * layerSize) + (y * width) + x];
	}
	
	public void setBlock(int blockID, int x, int y, int z, boolean rerender) {
		this.blocks[(z * layerSize) + (y * width) + x] = blockID;
		
		if (rerender) {
			createMesh();
			if (x == 0 && world.getBlockID(x + position.getBlockX() - 1, y + position.getBlockY(), z + position.getBlockZ()) != Blocks.AIR) {
				world.getChunk(position.getX() - 1, position.getY(), position.getZ()).createMesh();
			}
			if (y == 0 && world.getBlockID(x + position.getBlockX(), y + position.getBlockY() - 1, z + position.getBlockZ()) != Blocks.AIR) {
				world.getChunk(position.getX(), position.getY() - 1, position.getZ()).createMesh();
			}
			if (z == 0 && world.getBlockID(x + position.getBlockX(), y + position.getBlockY(), z + position.getBlockZ() - 1) != Blocks.AIR) {
				world.getChunk(position.getX(), position.getY(), position.getZ() - 1).createMesh();
			}
			if (x == width - 1 && world.getBlockID(x + position.getBlockX() + 1, y + position.getBlockY(), z + position.getBlockZ()) != Blocks.AIR) {
				world.getChunk(position.getX() + 1, position.getY(), position.getZ()).createMesh();
			}
			if (y == width - 1 && world.getBlockID(x + position.getBlockX(), y + position.getBlockY() + 1, z + position.getBlockZ()) != Blocks.AIR) {
				world.getChunk(position.getX(), position.getY() + 1, position.getZ()).createMesh();
			}
			if (z == width - 1 && world.getBlockID(x + position.getBlockX(), y + position.getBlockY(), z + position.getBlockZ() + 1) != Blocks.AIR) {
				world.getChunk(position.getX(), position.getY(), position.getZ() + 1).createMesh();
			}
		}
	}
	
	public ChunkMesh getChunkMesh() {
		if (this.chunkMesh == null) {
			createMesh();
		}
		return this.chunkMesh;
	}
	
	private Vector3i pos = new Vector3i(0, 0, 0);
	public void createMesh() {
		//long startTime = System.nanoTime();
		
		FloatBuffer vertices = MemoryUtil.memAllocFloat(65536 * 3);
		FloatBuffer texCoords = MemoryUtil.memAllocFloat(65536 * 2);
		FloatBuffer lighting = MemoryUtil.memAllocFloat(65536 * 3);
		IntBuffer indices = MemoryUtil.memAllocInt(65536);
		
		for (int i = 0; i < size; i++) {
			int blockID = blocks[i];
			if (blockID != Blocks.AIR) {
				Block block = Blocks.BLOCKS[blockID];
				
				int z = i / (layerSize);
				int y = (i - (z * layerSize)) / width;
				int x = i - ((z * layerSize) + (y * width));
				z += position.getBlockZ();
				y += position.getBlockY();
				x += position.getBlockX();
				pos.set(x, y, z);
				
				if (i - layerSize >= 0 && i + layerSize < size && (i % layerSize) - width >= 0 && (i % layerSize) + width < layerSize && (i % width) - 1 >= 0 && (i % width) + 1 < width) {
					if (!Blocks.BLOCKS[blocks[i - layerSize]].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.NORTH, vertices, texCoords, lighting, indices);
					}
					if (!Blocks.BLOCKS[blocks[i + layerSize]].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.SOUTH, vertices, texCoords, lighting, indices);
					}
					if (!Blocks.BLOCKS[blocks[i - width]].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.DOWN, vertices, texCoords, lighting, indices);
					}
					if (!Blocks.BLOCKS[blocks[i + width]].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.UP, vertices, texCoords, lighting, indices);
					}
					if (!Blocks.BLOCKS[blocks[i - 1]].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.WEST, vertices, texCoords, lighting, indices);
					}
					if (!Blocks.BLOCKS[blocks[i + 1]].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.EAST, vertices, texCoords, lighting, indices);
					}
				} else {
					if (!Blocks.BLOCKS[world.getBlockID(x, y, z - 1)].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.NORTH, vertices, texCoords, lighting, indices);
					}
					if (!Blocks.BLOCKS[world.getBlockID(x, y, z + 1)].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.SOUTH, vertices, texCoords, lighting, indices);
					}
					if (!Blocks.BLOCKS[world.getBlockID(x, y - 1, z)].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.DOWN, vertices, texCoords, lighting, indices);
					}
					if (!Blocks.BLOCKS[world.getBlockID(x, y + 1, z)].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.UP, vertices, texCoords, lighting, indices);
					}
					if (!Blocks.BLOCKS[world.getBlockID(x - 1, y, z)].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.WEST, vertices, texCoords, lighting, indices);
					}
					if (!Blocks.BLOCKS[world.getBlockID(x + 1, y, z)].isFullOpaqueCube()) {
						block.addToBuffer(pos, Direction.EAST, vertices, texCoords, lighting, indices);
					}
				}
				block.addToBuffer(pos, Direction.UNDEFINED, vertices, texCoords, lighting, indices);
			}
		}
		
		if (this.chunkMesh != null) {
			this.chunkMesh.updateMesh(vertices, texCoords, lighting, indices, indices.position());
		} else {
			this.chunkMesh = new ChunkMesh(vertices, texCoords, lighting, indices, indices.position());
		}
		
		//System.out.println("Chunk meshing took: " + ((System.nanoTime() - startTime) / 1000) + " millis");
	}
	
	public void cleanup() {
		if (this.chunkMesh != null) {
			this.chunkMesh.cleanup();
		}
	}

}

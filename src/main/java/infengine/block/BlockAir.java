package main.java.infengine.block;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.joml.Vector3i;

import main.java.infengine.model.blockmodel.IBlockModel;
import main.java.infengine.util.AxisAlignedBB;
import main.java.infengine.util.Direction;

public class BlockAir extends Block {
	
	public BlockAir() {
		super("air", "Air", new IBlockModel() {
			@Override
			public void addFaceQuadsToBuffer(Vector3i pos, Direction face, FloatBuffer vertices,
					FloatBuffer texCoords, FloatBuffer lighting, IntBuffer indices) {}
		});
	}
	
	@Override
	public boolean isFullCube() {
		return false;
	}
	
	@Override
	public boolean isTransparent() {
		return true;
	}
	
	public AxisAlignedBB getCollisionAABB() {
		return NULL_AABB;
	}

}

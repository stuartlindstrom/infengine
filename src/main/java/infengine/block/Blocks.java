package main.java.infengine.block;

public class Blocks {
	
	public static final int AIR = 0;
	public static final int STONE = 1;
	public static final int COBBLESTONE = 2;
	public static final int DIRT = 3;
	public static final int GRASS = 4;
	
	public static Block[] BLOCKS = new Block[] {
			new BlockAir(),
			new Block("stone", "Stone", 0),
			new Block("cobblestone", "Cobblestone", 1),
			new Block("dirt", "Dirt", 2),
			new Block("grass", "Grass", 2, 4, 3),
			new Block("gravel", "Gravel", 5),
			new Block("obsidian", "Obsidian", 7),
			new Block("stone_brick", "Stone Brick", 10),
			new Block("carved_stone_brick", "Carved Stone Brick", 8),
			new Block("brick", "Clay Brick", 13),
			new Block("log", "Log", 18, 18, 17),
			new Block("planks", "Wood Planks", 19),
	};
	
	/*
	private static int nextID = 0;
	public static int registerBlock(Block block) {
		int id = nextID;
		nextID++;
		System.out.println(block.getName() + ": " + id);
		BLOCKS[id] = block;
		
		System.out.println(block.getName() + ": " + id);
		return id;
	}
	*/

}

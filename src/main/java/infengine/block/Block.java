package main.java.infengine.block;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.joml.Vector3i;

import main.java.infengine.model.blockmodel.BasicBlockModel;
import main.java.infengine.model.blockmodel.IBlockModel;
import main.java.infengine.util.AxisAlignedBB;
import main.java.infengine.util.Direction;

public class Block {

	public static final AxisAlignedBB FULL_AABB = new AxisAlignedBB(0, 0, 0, 1, 1, 1);
	public static final AxisAlignedBB NULL_AABB = new AxisAlignedBB(0, 0, 0, 0, 0, 0);
	
	protected final String name;
	protected final String displayName;
	protected IBlockModel model;
	
	public Block(String name, String displayName, IBlockModel iBlockModel) {
		this.name = name;
		this.displayName = displayName;
		this.model = iBlockModel;
	}
	
	public Block(String name, String displayName, int textureIndex) {
		this(name, displayName, new BasicBlockModel(textureIndex));
	}
	
	public Block(String name, String displayName, int downTexIndex, int upTexIndex, int sideTexIndex) {
		this(name, displayName, new BasicBlockModel(downTexIndex, upTexIndex, sideTexIndex, sideTexIndex, sideTexIndex, sideTexIndex));
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getDisplayName() {
		return this.displayName;
	}
	
	public boolean isFullCube() {
		return true;
	}
	
	public boolean isTransparent() {
		return false;
	}
	
	public boolean isFullOpaqueCube() {
		return this.isFullCube() && !this.isTransparent();
	}
	
	public AxisAlignedBB getCollisionAABB() {
		return FULL_AABB;
	}
	
	public void addToBuffer(Vector3i pos, Direction face, FloatBuffer vertices, FloatBuffer texCoords, FloatBuffer lighting, IntBuffer indices) {
		this.model.addFaceQuadsToBuffer(pos, face, vertices, texCoords, lighting, indices);
	}
	
	public Direction selects(double sX, double sY, double sZ, double eX, double eY, double eZ, int blockX, int blockY, int blockZ) {
		AxisAlignedBB collisionBB = this.getCollisionAABB();
		
		if (!collisionBB.isValid()) return Direction.UNDEFINED;
		
		return collisionBB.collideRayWithOffset(sX, sY, sZ, eX, eY, eZ, blockX, blockY, blockZ);
	}
	
}

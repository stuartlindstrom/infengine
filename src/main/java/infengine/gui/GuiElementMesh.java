package main.java.infengine.gui;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.system.MemoryUtil;

import main.java.infengine.render.Texture;
import main.java.infengine.render.Window;

public class GuiElementMesh {
	
	protected final int vaoId;
	protected final List<Integer> vboIds = new ArrayList<Integer>();
	protected final int vertexCount;
	protected Texture texture;
	
	public GuiElementMesh(float[] vertices, float[] texCoords, int[] indices, Texture texture) {
		FloatBuffer vertexBuffer = null;
		FloatBuffer textureBuffer = null;
		IntBuffer indexBuffer = null;
		try {
			vertexBuffer = MemoryUtil.memAllocFloat(vertices.length);
			vertexBuffer.put(vertices).flip();
			
			textureBuffer = MemoryUtil.memAllocFloat(texCoords.length);
			textureBuffer.put(texCoords).flip();
			
			indexBuffer = MemoryUtil.memAllocInt(indices.length);
			this.vertexCount = indices.length; 
			indexBuffer.put(indices).flip();
			
			// Create the vao and bind opengl to it
			this.vaoId = glGenVertexArrays();
			glBindVertexArray(this.vaoId);
		
			int vboId;
			
			// Creates the vertex vbo
			vboId = glGenBuffers();
			this.vboIds.add(vboId);
			glBindBuffer(GL_ARRAY_BUFFER, vboId);
			glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			
			// Creates the texture vbo
			vboId = glGenBuffers();
			this.vboIds.add(vboId);
			glBindBuffer(GL_ARRAY_BUFFER, vboId);
			glBufferData(GL_ARRAY_BUFFER, textureBuffer, GL_STATIC_DRAW);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
			
			// Creates the index vbo
			vboId = glGenBuffers();
			this.vboIds.add(vboId);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL_STATIC_DRAW);
		
			// Unbind the vao and vbo objects from opengl
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		} finally {
			if (vertexBuffer != null) {
				MemoryUtil.memFree(vertexBuffer);
			}
			if (textureBuffer != null) {
				MemoryUtil.memFree(textureBuffer);
			}
			if (indexBuffer != null) {
				MemoryUtil.memFree(indexBuffer);
			}
		}
	}
	
	public boolean isEmpty() {
		return this.vertexCount == 0;
	}
	
	public void render(Window window) {
		glActiveTexture(GL_TEXTURE0);
		texture.bind();
		
		glBindVertexArray(this.vaoId);
		
		// Enable the coordinate vbo
		glEnableVertexAttribArray(0);
		// Enable the texture vbo
		glEnableVertexAttribArray(1);
		
		glDrawElements(GL_TRIANGLES, this.vertexCount, GL_UNSIGNED_INT, 0);
		
		// Disable the coordinate vbo
		glDisableVertexAttribArray(0);
		// Disable the texture vbo
		glDisableVertexAttribArray(1);
		
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public void cleanup() {
		glDisableVertexAttribArray(0);
		
		// Delete the vbos
		for (int vboId : this.vboIds) {
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glDeleteBuffers(vboId);
		}
		
		// Delete the vao
		glBindVertexArray(0);
        glDeleteVertexArrays(vaoId);
	}

}

package main.java.infengine.gui;

import org.joml.Matrix4f;
import org.joml.Vector4f;

import main.java.infengine.InfEngine;
import main.java.infengine.block.Blocks;
import main.java.infengine.entity.mob.player.EntityPlayer;
import main.java.infengine.model.Mesh;
import main.java.infengine.render.ShaderProgram;
import main.java.infengine.render.Texture;
import main.java.infengine.render.Transformation;
import main.java.infengine.render.Window;
import main.java.infengine.util.StringIOUtils;

public class Hud {
	
	public static Texture HUD_TEXTURES;
	static {
		try {
			HUD_TEXTURES = new Texture("src/main/resources/textures/hud.png");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private ShaderProgram shader;
	
	private float[] crosshairVertices;
	private float[] crosshairTexCoords;
	private int[] crosshairIndices;
	private Mesh crosshairs;
	
	private int prevWindowHeight;
	private int prevWindowWidth;
	
	private StringMesh fpsIndicator;
	private int prevFPS;
	private StringMesh blockIndicator;
	private int prevBlock;
	private StringMesh positionIndicator;
	private StringMesh collisionIndicator;
	private StringMesh flightIndicator;
	
	private FontRenderer fontRenderer;
	
	public Hud(Window window) throws Exception {
		prevWindowHeight = window.getHeight();
		prevWindowWidth = window.getWidth();
		
		this.fontRenderer = InfEngine.infEngine.fontRenderer;
		
		//System.out.println(window.getWidth() + ", " + window.getHeight());
		crosshairVertices = new float[] {
				window.getWidth() * 0.5f - 16, window.getHeight() * 0.5f - 16, 0,
				window.getWidth() * 0.5f - 16, window.getHeight() * 0.5f + 16, 0,
				window.getWidth() * 0.5f + 16, window.getHeight() * 0.5f + 16, 0,
				window.getWidth() * 0.5f + 16, window.getHeight() * 0.5f - 16, 0,
		};
		crosshairTexCoords = new float[] {
				0, 0,
				0, 0.0625f,
				0.0625f, 0.0625f,
				0.0625f, 0,
		};
		crosshairIndices = new int[] { 0, 1, 3, 3, 1, 2, };
		
		crosshairs = new Mesh(crosshairVertices, new float[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, crosshairTexCoords, crosshairIndices, new Texture("src/main/resources/textures/hud.png"));
		
		fpsIndicator = fontRenderer.createText("FPS: " + InfEngine.infEngine.getFPS(), 12, 12, 24, new Vector4f(1, 1, 1, 1));
		prevBlock = InfEngine.infEngine.currentBlockID;
		positionIndicator = fontRenderer.createText("Position: (0, 0, 0)", 12, 36, 24, new Vector4f(1, 1, 1, 1));
		collisionIndicator = fontRenderer.createText("Collision Enabled: ", 12, 72, 24, new Vector4f(0.875F, 0.875F, 0.875F, 1));
		flightIndicator = fontRenderer.createText("Flight Enabled: ", 12, 96, 24, new Vector4f(0.875F, 0.875F, 0.875F, 1));
		blockIndicator = fontRenderer.createText(Blocks.BLOCKS[prevBlock].getDisplayName(), (window.getWidth() / 2) - (fontRenderer.getWidth(Blocks.BLOCKS[prevBlock].getDisplayName(), 36) / 2), prevWindowHeight - (12 + 36), 36, new Vector4f(1, 1, 1, 1));
		
		this.shader = new ShaderProgram();
		this.shader.createVertexShader(StringIOUtils.loadResource("/main/resources/shaders/hud.vs"));
		this.shader.createFragmentShader(StringIOUtils.loadResource("/main/resources/shaders/hud.fs"));
		this.shader.link();
		
		this.shader.createUniform("orthoMatrix");
		
		this.shader.createUniform("texture_sampler");
	}
	
	public void render(Window window, Transformation transformation) {
		boolean repositionText = false;
		if (this.prevWindowHeight != window.getHeight() || this.prevWindowWidth != window.getWidth()) {
			this.prevWindowHeight = window.getHeight();
			this.prevWindowWidth = window.getWidth();
			crosshairVertices = new float[] {
					window.getWidth() * 0.5f - 16, window.getHeight() * 0.5f - 16, 0,
					window.getWidth() * 0.5f - 16, window.getHeight() * 0.5f + 16, 0,
					window.getWidth() * 0.5f + 16, window.getHeight() * 0.5f + 16, 0,
					window.getWidth() * 0.5f + 16, window.getHeight() * 0.5f - 16, 0,
			};
			crosshairs.cleanup();
			try {
				crosshairs = new Mesh(crosshairVertices, new float[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, crosshairTexCoords, crosshairIndices, new Texture("src/main/resources/textures/hud.png"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			repositionText = true;
		}
		
		shader.bind();
		
		Matrix4f orthoMatrix = transformation.getOrthoProjectionMatrix(0, window.getWidth(), window.getHeight(), 0);
		
		shader.setUniform("orthoMatrix", orthoMatrix);
		shader.setUniform("texture_sampler", 0);
		
		crosshairs.render();
		
		shader.unbind();
		
		int fps = InfEngine.infEngine.getFPS();
		if (prevFPS != fps) {
			fontRenderer.updateText("FPS: " + InfEngine.infEngine.getFPS(), 12, 12, 24, fpsIndicator);
		}
		prevFPS = fps;
		fpsIndicator.render(window, transformation);
		
		int currentBlock = InfEngine.infEngine.currentBlockID;
		if (prevBlock != currentBlock || repositionText) {
			String blockName = Blocks.BLOCKS[currentBlock].getDisplayName();
			fontRenderer.updateText(blockName, (window.getWidth() / 2) - (fontRenderer.getWidth(blockName, 36) / 2), window.getHeight() - (12 + 36), 36, blockIndicator);
		}
		blockIndicator.render(window, transformation);
		prevBlock = currentBlock;
		
		EntityPlayer player = InfEngine.infEngine.player;
		if (player != null/* && (player.x != player.prevX || player.y != player.prevY || player.z != player.prevZ)*/) {
			fontRenderer.updateText("Position: (" + String.format("%.2f", player.x) + ", " + String.format("%.2f", player.y) + ", " + String.format("%.2f", player.z) + ")", 12, 36, 24, positionIndicator);
		}
		positionIndicator.render(window, transformation);
		
		fontRenderer.updateText("Collision Enabled: " + (InfEngine.infEngine.player.noClip() ? "FALSE" : "TRUE"), 12, 72, 24, collisionIndicator);
		fontRenderer.updateText("Flight Enabled: " + (InfEngine.infEngine.player.hasGravity() ? "FALSE" : "TRUE"), 12, 96, 24, flightIndicator);
		collisionIndicator.render(window, transformation);
		flightIndicator.render(window, transformation);
	}
	
	public void cleanup() {
		if (shader != null) shader.cleanup();
		if (HUD_TEXTURES != null) HUD_TEXTURES.cleanup();
		if (crosshairs != null) crosshairs.cleanup();
		if (fpsIndicator != null) fpsIndicator.cleanup();
		if (blockIndicator != null) blockIndicator.cleanup();
		if (positionIndicator != null) positionIndicator.cleanup();
	}

}

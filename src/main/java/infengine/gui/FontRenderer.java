package main.java.infengine.gui;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.joml.Vector4f;
import org.lwjgl.system.MemoryUtil;

import main.java.infengine.render.ShaderProgram;
import main.java.infengine.render.Texture;
import main.java.infengine.util.StringIOUtils;

public class FontRenderer {
	
	private static final String font_name = "romulus";
	
	private Texture asciiSheet;
	private Float[] charWidths;
	public ShaderProgram shader;
	
	public FontRenderer() throws Exception {
		this.asciiSheet = new Texture("src/main/resources/textures/font/" + font_name + ".png");
		String[] widthFile = StringIOUtils.readLines("/main/resources/textures/font/" + font_name + "_widths.txt");
		this.charWidths = new Float[widthFile.length];
		for (int i = 0; i < widthFile.length; i++) {
			charWidths[i] = Float.parseFloat(widthFile[i]);
		}
		
		this.shader = new ShaderProgram();
		this.shader.createVertexShader(StringIOUtils.loadResource("/main/resources/shaders/font.vs"));
		this.shader.createFragmentShader(StringIOUtils.loadResource("/main/resources/shaders/font.fs"));
		this.shader.link();
		
		this.shader.createUniform("orthoMatrix");
		this.shader.createUniform("texture_sampler");
		this.shader.createUniform("color");
	}
	
	public StringMesh updateText(String text, float x, float y, int fontSize, StringMesh mesh) {
		int length = text.length();
		FloatBuffer vertexBuffer = MemoryUtil.memAllocFloat(length * 8);
		FloatBuffer textureBuffer = MemoryUtil.memAllocFloat(length * 8);
		IntBuffer indexBuffer = MemoryUtil.memAllocInt(length * 6);
		
		char[] chars = text.toCharArray();
		
		int xOffset = 0;
		for (int i = 0; i < chars.length; i++) {
			int texIndex = chars[i];
			float charWidth =  texIndex < charWidths.length && texIndex > 0 ? charWidths[texIndex] : 1;
			
			if (texIndex < 256) {
				vertexBuffer.put(xOffset + x);			vertexBuffer.put(fontSize + y);
				vertexBuffer.put(xOffset + x);			vertexBuffer.put(y);
				vertexBuffer.put((charWidth * fontSize) + xOffset + x);		vertexBuffer.put(y);
				vertexBuffer.put((charWidth * fontSize) + xOffset + x);		vertexBuffer.put(fontSize + y);
			
				float u = (texIndex % 16) * 0.0625f;
				float v = (texIndex / 16) * 0.0625f;
				textureBuffer.put(u);					textureBuffer.put(v + 0.0625f);
				textureBuffer.put(u);					textureBuffer.put(v);
				textureBuffer.put(u + (0.0625f * charWidth));			textureBuffer.put(v);
				textureBuffer.put(u + (0.0625f * charWidth));			textureBuffer.put(v + 0.0625f);
				
				indexBuffer.put((i * 4));			indexBuffer.put((i * 4) + 1);		indexBuffer.put((i * 4) + 3);
				indexBuffer.put((i * 4) + 3);		indexBuffer.put((i * 4) + 1);		indexBuffer.put((i * 4) + 2);
			}
			xOffset += (charWidth * fontSize) + 1;
		}
		
		if (mesh != null) {
			mesh.streamData(vertexBuffer, textureBuffer, indexBuffer, indexBuffer.position(), xOffset - 1);
		} else {
			mesh = new StringMesh(vertexBuffer, textureBuffer, indexBuffer, indexBuffer.position(), asciiSheet, new Vector4f(1, 1, 1, 1), xOffset - 1);
		}
		
		return mesh;
	}
	
	public StringMesh createText(String text, float x, float y, int fontSize, Vector4f color) {
		int length = text.length();
		FloatBuffer vertexBuffer = MemoryUtil.memAllocFloat(length * 8);
		FloatBuffer textureBuffer = MemoryUtil.memAllocFloat(length * 8);
		IntBuffer indexBuffer = MemoryUtil.memAllocInt(length * 6);		
		
		char[] chars = text.toCharArray();
		
		int xOffset = 0;
		for (int i = 0; i < chars.length; i++) {
			int texIndex = chars[i];
			float charWidth =  texIndex < charWidths.length && texIndex > 0 ? charWidths[texIndex] : 1;
			
			if (texIndex < 256) {
				vertexBuffer.put(xOffset + x);			vertexBuffer.put(fontSize + y);
				vertexBuffer.put(xOffset + x);			vertexBuffer.put(y);
				vertexBuffer.put((charWidth * fontSize) + xOffset + x);		vertexBuffer.put(y);
				vertexBuffer.put((charWidth * fontSize) + xOffset + x);		vertexBuffer.put(fontSize + y);
			
				float u = (texIndex % 16) * 0.0625f;
				float v = (texIndex / 16) * 0.0625f;
				textureBuffer.put(u);					textureBuffer.put(v + 0.0625f);
				textureBuffer.put(u);					textureBuffer.put(v);
				textureBuffer.put(u + (0.0625f * charWidth));			textureBuffer.put(v);
				textureBuffer.put(u + (0.0625f * charWidth));			textureBuffer.put(v + 0.0625f);
				
				indexBuffer.put((i * 4));			indexBuffer.put((i * 4) + 1);		indexBuffer.put((i * 4) + 3);
				indexBuffer.put((i * 4) + 3);		indexBuffer.put((i * 4) + 1);		indexBuffer.put((i * 4) + 2);
			}
			xOffset += (charWidth * fontSize) + 1;
		}
		
		StringMesh mesh = new StringMesh(vertexBuffer, textureBuffer, indexBuffer, indexBuffer.position(), asciiSheet, color, xOffset - 1);
		
		return mesh;
	}
	
	public int getWidth(String text, int fontSize) {
		char[] chars = text.toCharArray();
		
		int xOffset = 0;
		for (int i = 0; i < chars.length; i++) {
			int c = chars[i];
			float charWidth =  c < charWidths.length && c > 0 ? charWidths[c] : 1;
			xOffset += (charWidth * fontSize) + 1;
		}
		
		return xOffset - 1;
	}
	
	public void cleanup() {
		if (this.asciiSheet != null) this.asciiSheet.cleanup();
		if (this.shader != null) this.shader.cleanup();
	}

}

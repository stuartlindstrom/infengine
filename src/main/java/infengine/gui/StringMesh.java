package main.java.infengine.gui;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.joml.Matrix4f;
import org.joml.Vector4f;
import org.lwjgl.system.MemoryUtil;

import main.java.infengine.InfEngine;
import main.java.infengine.render.ShaderProgram;
import main.java.infengine.render.Texture;
import main.java.infengine.render.Transformation;
import main.java.infengine.render.Window;

public class StringMesh {

	protected final int vaoId;
	protected final List<Integer> vboIds = new ArrayList<Integer>();
	protected int vertexCount;
	protected Texture texture;
	
	protected Vector4f color;
	protected int width; // in pixels
	
	// DON'T PASS THIS CONSTRUCTOR A TEXTURE THAT WILL NEED TO BE CLEANED UP!
	public StringMesh(FloatBuffer vertexBuffer, FloatBuffer textureBuffer, IntBuffer indexBuffer, int vertexCount, Texture texture, Vector4f color, int width) {
		try {
			if (vertexCount > 0) {
				vertexBuffer.flip();
				textureBuffer.flip();
				indexBuffer.flip();
				
				this.texture = texture;
				this.width = width;
				this.vertexCount = vertexCount;
				this.color = color;
				
				// Create the vao and bind opengl to it
				this.vaoId = glGenVertexArrays();
				glBindVertexArray(this.vaoId);
			
				int vboId;
				
				// Creates the vertex vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ARRAY_BUFFER, vboId);
				glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);
				
				// Creates the texture vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ARRAY_BUFFER, vboId);
				glBufferData(GL_ARRAY_BUFFER, textureBuffer, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
				
				// Creates the index vbo
				vboId = glGenBuffers();
				this.vboIds.add(vboId);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL_DYNAMIC_DRAW);
			
				// Unbind the vao and vbo objects from opengl
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);
			} else {
				this.vertexCount = 0;
				this.vaoId = -1;
			}
		} finally {
			if (vertexBuffer != null) {
				MemoryUtil.memFree(vertexBuffer);
			}
			if (textureBuffer != null) {
				MemoryUtil.memFree(textureBuffer);
			}
			if (indexBuffer != null) {
				MemoryUtil.memFree(indexBuffer);
			}
		}
	}
	
	public void streamData(FloatBuffer vertexBuffer, FloatBuffer textureBuffer, IntBuffer indexBuffer, int vertexCount, int width) {
		try {
			int vertexVBO = this.vboIds.get(0);
			int textureVBO = this.vboIds.get(1);
			int indexVBO = this.vboIds.get(2);
			
			vertexBuffer.flip();
			textureBuffer.flip();
			indexBuffer.flip();
			
			int prevVertexCount = this.vertexCount;
			
			this.vertexCount = vertexCount;
			this.width = width;
			
			glBindVertexArray(this.vaoId);
		
			glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
			//glMapBufferRange(GL_ARRAY_BUFFER, 0, 1, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
			glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_DYNAMIC_DRAW);
			
			glBindBuffer(GL_ARRAY_BUFFER, textureVBO);
			//glMapBufferRange(GL_ARRAY_BUFFER, 0, 1, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
			glBufferData(GL_ARRAY_BUFFER, textureBuffer, GL_DYNAMIC_DRAW);
			
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexVBO);
			//glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, 1, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL_DYNAMIC_DRAW);
			
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		} finally {
			if (vertexBuffer != null) {
				MemoryUtil.memFree(vertexBuffer);
			}
			if (textureBuffer != null) {
				MemoryUtil.memFree(textureBuffer);
			}
			if (indexBuffer != null) {
				MemoryUtil.memFree(indexBuffer);
			}
		}
	}
	
	public boolean isEmpty() {
		return this.vertexCount == 0;
	}
	
	public Vector4f getColor() {
		return this.color;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public void setColor(float r, float g, float b, float a) {
		this.color.set(r, g, b, a);
	}
	
	public void render(Window window, Transformation transformation) {
		ShaderProgram shader = InfEngine.infEngine.fontRenderer.shader;
		shader.bind();
		
		Matrix4f orthoMatrix = transformation.getOrthoProjectionMatrix(0, window.getWidth(), window.getHeight(), 0);
		
		shader.setUniform("orthoMatrix", orthoMatrix);
		shader.setUniform("texture_sampler", 0);
		shader.setUniform("color", this.color);
		
		glActiveTexture(GL_TEXTURE0);
		texture.bind();
		
		glBindVertexArray(this.vaoId);
		
		// Enable vbos
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		
		glDrawElements(GL_TRIANGLES, this.vertexCount, GL_UNSIGNED_INT, 0);
		
		// Disable vbos
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
		
		shader.unbind();
	}
	
	public void cleanup() {
		glDisableVertexAttribArray(0);
		
		// Delete the vbos
		for (int vboId : this.vboIds) {
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glDeleteBuffers(vboId);
		}
		
		// Delete the vao
		glBindVertexArray(0);
        glDeleteVertexArrays(vaoId);
	}
	
}

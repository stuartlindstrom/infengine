package main.java.infengine.util;

public class Timer {

	private double prevTickTime;

	public void init() {
		prevTickTime = getTime();
	}

	public double getTime() {
		return System.nanoTime() / 1_000_000_000.0;
	}

	public float getElapsedTime() {
		double time = getTime();
		float elapsedTime = (float) (time - prevTickTime);
		prevTickTime = time;
		return elapsedTime;
	}

	public double getLastLoopTime() {
		return prevTickTime;
	}

}

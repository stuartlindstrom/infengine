package main.java.infengine.util;

import java.util.Random;

import org.joml.Vector2d;
import org.joml.Vector3d;

public class NoiseUtils {
	
	private static final Vector2d[] vecPool2d = new Vector2d[] {
			new Vector2d(0.9238795042037964, 0.3826834559440613),
			new Vector2d(0.3826834261417389, 0.9238795042037964),
			new Vector2d(-0.3826833963394165, 0.9238795638084412),
			new Vector2d(-0.9238795042037964, 0.38268348574638367),
			new Vector2d(-0.9238795042037964, -0.3826834261417389),
			new Vector2d(-0.38268357515335083, -0.9238795042037964),
			new Vector2d(0.3826836049556732, -0.9238794445991516),
			new Vector2d(0.9238795638084412, -0.3826834261417389) };
	private static final Vector3d[] vecPool3d = new Vector3d[] {
			new Vector3d(1,1,0),
			new Vector3d(-1,1,0),
			new Vector3d(1,-1,0),
			new Vector3d(-1,-1,0),
			new Vector3d(1,0,1),
			new Vector3d(-1,0,1),
			new Vector3d(1,0,-1),
			new Vector3d(-1,0,-1),
			new Vector3d(0,1,1),
			new Vector3d(0,-1,1),
			new Vector3d(0,1,-1),
			new Vector3d(0,-1,-1) };

	private static final Random rand = new Random();

	public static double getNoise(int x, int y, long seed) {
		return getNoise(x, y, seed, 83, 1, 0.4, 4);
	}
	
	public static double getNoise(int x, int y, long seed, double frequency, double amplitude, double persistence, int octaves) {
		double max = 0;
		double noise = 0;

		for (int i = 0; i < octaves; i++) {
			noise += getOctave(x, y, frequency, seed) * amplitude;

			max += amplitude;
			amplitude *= persistence;
			frequency /= 2;
		}
		noise /= max;

		return noise;
	}
	
	public static double getNoise(int x, int y, int z, long seed, double frequency, double amplitude, double persistence, int octaves) {
		double max = 0;
		double noise = 0;

		for (int i = 0; i < octaves; i++) {
			noise += getOctave(x, y, z, frequency, seed) * amplitude;

			max += amplitude;
			amplitude *= persistence;
			frequency /= 2;
		}
		noise /= max;

		return noise;
	}

	private static double getOctave(int x, int y, double frequency, long seed) {
		int x0 = (int) Math.floor(x / frequency);
		int y0 = (int) Math.floor(y / frequency);
		double relX = x / frequency;
		double relY = y / frequency;
		double u = relX - x0;
		double v = relY - y0;

		double x1 = lerp((dot(getGradient(x0, y0, seed), u, v) + 1) / 2,
				(dot(getGradient(x0 + 1, y0, seed), u - 1, v) + 1) / 2, fade(u));
		double x2 = lerp((dot(getGradient(x0, y0 + 1, seed), u, v - 1) + 1) / 2,
				(dot(getGradient(x0 + 1, y0 + 1, seed), u - 1, v - 1) + 1) / 2, fade(u));
		return lerp(x1, x2, fade(v));
	}
	
	private static double dot(Vector2d v, double x, double y) {
		return v.x * x + v.y * y;
	}
	
	private static double getOctave(int x, int y, int z, double frequency, long seed) {
		int x0 = (int) Math.floor(x / frequency);
		int y0 = (int) Math.floor(y / frequency);
		int z0 = (int) Math.floor(z / frequency);
		double relX = x / frequency;
		double relY = y / frequency;
		double relZ = z / frequency;
		double u = relX - x0;
		double v = relY - y0;
		double w = relZ - z0;

		double x1 = lerp((getGradient(x0, y0, z0, seed).dot(u, v, w) + 1) / 2,
				(getGradient(x0 + 1, y0, z0, seed).dot(u - 1, v, w) + 1) / 2, fade(u));
		double x2 = lerp((getGradient(x0, y0 + 1, z0, seed).dot(u, v - 1, w) + 1) / 2,
				(getGradient(x0 + 1, y0 + 1, z0, seed).dot(u - 1, v - 1, w) + 1) / 2, fade(u));
		double y1 = lerp(x1, x2, fade(v));
		double x3 = lerp((getGradient(x0, y0, z0 + 1, seed).dot(u, v, w - 1) + 1) / 2,
				(getGradient(x0 + 1, y0, z0 + 1, seed).dot(u - 1, v, w - 1) + 1) / 2, fade(u));
		double x4 = lerp((getGradient(x0, y0 + 1, z0 + 1, seed).dot(u, v - 1, w - 1) + 1) / 2,
				(getGradient(x0 + 1, y0 + 1, z0 + 1, seed).dot(u - 1, v - 1, w - 1) + 1) / 2, fade(u));
		double y2 = lerp(x3, x4, fade(v));
		
		return lerp(y1, y2, fade(w));
	}

	private static Vector2d getGradient(int x, int y, long seed) {
		rand.setSeed(simple_hash((int) seed, (int) (seed << 32), (int) Math.signum(y) * 512 + 512,
				(int) Math.signum(x) * 512 + 512, x, y));
		return vecPool2d[rand.nextInt(vecPool2d.length)];
	}
	
	private static Vector3d getGradient(int x, int y, int z, long seed) {
		rand.setSeed(simple_hash((int) seed, (int) (seed << 32), (int) Math.signum(z) * 512 + 512,
				(int) Math.signum(y) * 512 + 512, (int) Math.signum(x) * 512 + 512, x, y, z));
		return vecPool3d[rand.nextInt(vecPool3d.length)];
	}

	private static int simple_hash(int upperSeed, int lowerSeed, int sigY512, int sigX512, int x, int y) {
		int hash = 80238287;
		
		hash = (hash << 4) ^ (hash >> 28) ^ (upperSeed * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (lowerSeed * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (sigY512 * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (sigX512 * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (x * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (y * 5449 % 130651);
		
		return hash % 75327403;
	}
	
	private static int simple_hash(int upperSeed, int lowerSeed, int sigZ512, int sigY512, int sigX512, int x, int y, int z) {
		int hash = 80238287;
		
		hash = (hash << 4) ^ (hash >> 28) ^ (upperSeed * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (lowerSeed * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (sigZ512 * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (sigY512 * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (sigX512 * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (x * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (y * 5449 % 130651);
		hash = (hash << 4) ^ (hash >> 28) ^ (z * 5449 % 130651);
		
		return hash % 75327403;
	}
	
	/*
	private static int simple_hash(int... is) {
		int i;
		int hash = 80238287;

		for (i = 0; i < is.length; i++) {
			hash = (hash << 4) ^ (hash >> 28) ^ (is[i] * 5449 % 130651);
		}

		return hash % 75327403;
	}
	*/

	private static double lerp(double a, double b, double x) {
		return a + x * (b - a);
	}

	private static double fade(double d) {
		return d * d * d * ((d * ((d * 6) - 15)) + 10);
	}

}

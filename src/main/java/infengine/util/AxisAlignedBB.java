package main.java.infengine.util;

import org.joml.Intersectionf;
import org.joml.Vector2f;
import org.joml.Vector3d;
import org.joml.Vector3f;

public class AxisAlignedBB {
	
	public double minX, minY, minZ;
	public double maxX, maxY, maxZ;
	
	public AxisAlignedBB(double minX, double minY, double minZ, double maxX, double maxY, double maxZ) {
		this.minX = minX;
		this.minY = minY;
		this.minZ = minZ;
		this.maxX = maxX;
		this.maxY = maxY;
		this.maxZ = maxZ;
	}
	
	public void setBounds(double minX, double minY, double minZ, double maxX, double maxY, double maxZ) {
		this.minX = minX;
		this.minY = minY;
		this.minZ = minZ;
		this.maxX = maxX;
		this.maxY = maxY;
		this.maxZ = maxZ;
	}
	
	public AxisAlignedBB translated(double dX, double dY, double dZ) {
		return new AxisAlignedBB(minX + dX, minY + dY, minZ + dZ, maxX + dX, maxY + dY, maxZ + dZ);
	}
	
	public void translate(double dX, double dY, double dZ) {
		this.minX += dX;
		this.minY += dY;
		this.minZ += dZ;
		this.maxX += dX;
		this.maxY += dY;
		this.maxZ += dZ;
	}
	
	public void setFromTranslated(AxisAlignedBB orig, double dX, double dY, double dZ) {
		this.minX = orig.minX + dX;
		this.minY = orig.minY + dY;
		this.minZ = orig.minZ + dZ;
		this.maxX = orig.maxX + dX;
		this.maxY = orig.maxY + dY;
		this.maxZ = orig.maxZ + dZ;
	}
	
	public boolean intersects(double minXB, double minYB, double minZB, double maxXB, double maxYB, double maxZB) {
		return this.minX < maxXB && this.minY < maxYB && this.minZ < maxZB && this.maxX > minXB && this.maxY > minYB && this.maxZ > minZB;
	}
	
	public boolean intersects(AxisAlignedBB aabb) {
		return aabb.isValid() && this.intersects(aabb.minX, aabb.minY, aabb.minZ, aabb.maxX, aabb.maxY, aabb.maxZ);
	}
	
	public boolean intersectsRay(Vector3d pos, Vector3f dir, Vector2f result) {
		return Intersectionf.intersectRayAab((float) pos.x, (float) pos.y, (float) pos.z, dir.x, dir.y, dir.z, (float) minX, (float) minY, (float) minZ, (float) maxX, (float) maxY, (float) maxZ, result);
	}
	
	private Vector3d tempVec = new Vector3d();
	public Direction collideRayWithOffset(double sX, double sY, double sZ, double eX, double eY, double eZ, double oX, double oY, double oZ) {
		Direction hitSide = Direction.UNDEFINED;
		double minDistSq = Double.POSITIVE_INFINITY;
		
		if (MathUtil.interAtY(this.minY + oY, sX, sY, sZ, eX, eY, eZ, tempVec)) {
			if (tempVec.z >= this.minZ + oZ && tempVec.z <= maxZ + oZ
					&& tempVec.x >= this.minX + oX && tempVec.x <= this.maxX + oX
					&& tempVec.distanceSquared(sX, sY, sZ) < minDistSq) {
				minDistSq = tempVec.distanceSquared(sX, sY, sZ);
				hitSide = Direction.DOWN;
			}
		}
		if (MathUtil.interAtY(this.maxY + oY, sX, sY, sZ, eX, eY, eZ, tempVec)) {
			if (tempVec.z >= this.minZ + oZ && tempVec.z <= maxZ + oZ
					&& tempVec.x >= this.minX + oX && tempVec.x <= this.maxX + oX
					&& tempVec.distanceSquared(sX, sY, sZ) < minDistSq) {
				minDistSq = tempVec.distanceSquared(sX, sY, sZ);
				hitSide = Direction.UP;
			}
		}
		if (MathUtil.interAtZ(this.minZ + oZ, sX, sY, sZ, eX, eY, eZ, tempVec)) {
			if (tempVec.x >= this.minX + oX && tempVec.x <= maxX + oX
					&& tempVec.y >= this.minY + oY && tempVec.y <= this.maxY + oY
					&& tempVec.distanceSquared(sX, sY, sZ) < minDistSq) {
				minDistSq = tempVec.distanceSquared(sX, sY, sZ);
				hitSide = Direction.NORTH;
			}
		}
		if (MathUtil.interAtZ(this.maxZ + oZ, sX, sY, sZ, eX, eY, eZ, tempVec)) {
			if (tempVec.x >= this.minX + oX && tempVec.x <= maxX + oX
					&& tempVec.y >= this.minY + oY && tempVec.y <= this.maxY + oY
					&& tempVec.distanceSquared(sX, sY, sZ) < minDistSq) {
				minDistSq = tempVec.distanceSquared(sX, sY, sZ);
				hitSide = Direction.SOUTH;
			}
		}
		if (MathUtil.interAtX(this.minX + oX, sX, sY, sZ, eX, eY, eZ, tempVec)) {
			if (tempVec.z >= this.minZ + oZ && tempVec.z <= maxZ + oZ
					&& tempVec.y >= this.minY + oY && tempVec.y <= this.maxY + oY
					&& tempVec.distanceSquared(sX, sY, sZ) < minDistSq) {
				minDistSq = tempVec.distanceSquared(sX, sY, sZ);
				hitSide = Direction.WEST;
			}
		}
		if (MathUtil.interAtX(this.maxX + oX, sX, sY, sZ, eX, eY, eZ, tempVec)) {
			if (tempVec.z >= this.minZ + oZ && tempVec.z <= maxZ + oZ
					&& tempVec.y >= this.minY + oY && tempVec.y <= this.maxY + oY
					&& tempVec.distanceSquared(sX, sY, sZ) < minDistSq) {
				minDistSq = tempVec.distanceSquared(sX, sY, sZ);
				hitSide = Direction.EAST;
			}
		}
		
		return hitSide;
	}
	
	public boolean isValid() {
		return minX < maxX && minY < maxY && minZ < maxZ;
	}
	
	public boolean intersectsWithOffsets(AxisAlignedBB aabb, double xOffsetA, double yOffsetA, double zOffsetA, double xOffsetB, double yOffsetB, double zOffsetB) {
		return aabb.isValid() && 
				this.minX + xOffsetA < aabb.maxX + xOffsetB && 
				this.minY + yOffsetA < aabb.maxY + yOffsetB && 
				this.minZ + zOffsetA < aabb.maxZ + zOffsetB && 
				this.maxX + xOffsetA > aabb.minX + xOffsetB && 
				this.maxY + yOffsetA > aabb.minY + yOffsetB && 
				this.maxZ + zOffsetA > aabb.minZ + zOffsetB;
	}
	
	public double calcYOffset(AxisAlignedBB aabb, double defaultOffset) {
		if (isValid() && minX < aabb.maxX && maxX > aabb.minX && minZ < aabb.maxZ && maxZ > aabb.minZ) {
			if (defaultOffset < 0 && maxY <= aabb.minY) {
				double offset = maxY - aabb.minY;
				return offset > defaultOffset ? offset : defaultOffset;
			} else if (defaultOffset > 0 && minY >= aabb.maxY) {
				double offset = minY - aabb.maxY;
				return offset < defaultOffset ? offset : defaultOffset;
			}
		}
		return defaultOffset;
	}
	
	public double calcZOffset(AxisAlignedBB aabb, double defaultOffset) {
		if (isValid() && minX < aabb.maxX && maxX > aabb.minX && minY < aabb.maxY && maxY > aabb.minY) {
			if (defaultOffset < 0 && maxZ <= aabb.minZ) {
				double offset = maxZ - aabb.minZ;
				return offset > defaultOffset ? offset : defaultOffset;
			} else if (defaultOffset > 0 && minZ >= aabb.maxZ) {
				double offset = minZ - aabb.maxZ;
				return offset < defaultOffset ? offset : defaultOffset;
			}
		}
		return defaultOffset;
	}
	
	public double calcXOffset(AxisAlignedBB aabb, double defaultOffset) {
		if (isValid() && minY < aabb.maxY && maxY > aabb.minY && minZ < aabb.maxZ && maxZ > aabb.minZ) {
			if (defaultOffset < 0 && maxX <= aabb.minX) {
				double offset = maxX - aabb.minX;
				return offset > defaultOffset ? offset : defaultOffset;
			} else if (defaultOffset > 0 && minX >= aabb.maxX) {
				double offset = minX - aabb.maxX;
				return offset < defaultOffset ? offset : defaultOffset;
			}
		}
		return defaultOffset;
	}

}

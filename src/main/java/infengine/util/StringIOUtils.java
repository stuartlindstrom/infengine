package main.java.infengine.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class StringIOUtils {

	public static String loadResource(String fileName) {
		String content = "";
		try {
			BufferedReader fileReader = new BufferedReader(
					new InputStreamReader(StringIOUtils.class.getClass().getResourceAsStream(fileName)));
			String line;
			while ((line = fileReader.readLine()) != null) {
				content += line + "\n";
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}
	
	public static String[] readLines(String fileName) {
		List<String> content = new ArrayList<String>();
		try {
			BufferedReader fileReader = new BufferedReader(
					new InputStreamReader(StringIOUtils.class.getClass().getResourceAsStream(fileName)));
			String line;
			while ((line = fileReader.readLine()) != null) {
				content.add(line);
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content.toArray(new String[content.size()]);
	}

}

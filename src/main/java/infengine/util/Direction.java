package main.java.infengine.util;

public enum Direction {
	
	DOWN(0),
	UP(1),
	NORTH(2),
	SOUTH(3),
	WEST(4),
	EAST(5),
	UNDEFINED(6);
	
	private static Direction[] directions = new Direction[] {
			DOWN, UP, NORTH, SOUTH, WEST, EAST
	};
	private int index;
	
	private Direction(int index) {
		this.index = index;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public static Direction fromIndex(int i) {
		if (i >= 0 && i < directions.length) {
			return directions[i];
		}
		return UNDEFINED;
	}
	
	public static Direction getOpposite(Direction orig) {
		switch (orig) {
		case DOWN:
			return UP;
		case UP:
			return DOWN;
		case NORTH:
			return SOUTH;
		case SOUTH:
			return NORTH;
		case WEST:
			return EAST;
		case EAST:
			return WEST;
		default:
			return UNDEFINED;
		}
	}

}

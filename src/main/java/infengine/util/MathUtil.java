package main.java.infengine.util;

import org.joml.Vector3d;

public class MathUtil {

	public static int mod(int a, int b) {
		return (a % b + b) % b;
	}
	
	public static boolean interAtX(double x, double sX, double sY, double sZ, double eX, double eY, double eZ, Vector3d result) {
		double dX = eX - sX;
		double dY = eY - sY;
		double dZ = eZ - sZ;
		
		double iS = (x - sX) / dX;
		if (iS > 0 && iS <= 1) {
			result.set(sX + (dX * iS), sY + (dY * iS), sZ + (dZ * iS));
			return true;
		}
		result.set(sX, sY, sZ);
		return false;
	}
	
	public static boolean interAtY(double y, double sX, double sY, double sZ, double eX, double eY, double eZ, Vector3d result) {
		double dX = eX - sX;
		double dY = eY - sY;
		double dZ = eZ - sZ;
		
		double iS = (y - sY) / dY;
		if (iS > 0 && iS <= 1) {
			result.set(sX + (dX * iS), sY + (dY * iS), sZ + (dZ * iS));
			return true;
		}
		result.set(sX, sY, sZ);
		return false;
	}
	
	public static boolean interAtZ(double z, double sX, double sY, double sZ, double eX, double eY, double eZ, Vector3d result) {
		double dX = eX - sX;
		double dY = eY - sY;
		double dZ = eZ - sZ;
		
		double iS = (z - sZ) / dZ;
		if (iS > 0 && iS <= 1) {
			result.set(sX + (dX * iS), sY + (dY * iS), sZ + (dZ * iS));
			return true;
		}
		result.set(sX, sY, sZ);
		return false;
	}
	
}

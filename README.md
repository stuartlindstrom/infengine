# InfEngine

A "voxel" (grid-based blocky polygonal) engine written in Java using LWJGL 3.

### Setting Up

In Eclipse, go to File > Import > Gradle > Gradle Project, then select the root folder of your local repo as the project root folder and click 'Finish'. After all of the dependencies have been downloaded, make sure that the only source folder in the project's build path is /src. Once that's done, you should be able to run and debug the program.

### Images

![](images/26.png)

![](images/21.png)

![](images/19.png)

![](images/18.png)

## Disclaimer

This is a work in progress. Almost all of this was written by the version of me that existed a year ago, and that guy was an idiot. I am not responsible for any mental or physical harm caused by viewing the code in this repository.